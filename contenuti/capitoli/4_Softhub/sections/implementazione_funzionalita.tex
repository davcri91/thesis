Per illustrare come è stato utilizzato il framework, mostrerò il codice
di Softhub relativo all'
implementazione della funzionalità di upload delle applicazioni, con relativa
creazione della pagina dei dettagli. In alcuni casi non mostrerò il codice
completo del software, rimuovendo alcune righe di minor importanza,
per alleggerire la trattazione.

Il nucleo di un progetto Django sono i models, che rappresentano
la sorgente univoca per le informazioni riguardanti l'applicazione, in accordo
con il principio DRY che abbiamo esplorato nel precedente capitolo.
Dunque analizziamo le classi che sono state realizzate:

\begin{minted}[frame=lines, label=Application.py, linenos]{python}
def upload_dir(app, filename):
    path = ('applications/' + app.name + '/icons/' + filename)
    return path

class Application(models.Model):
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=500)
    website = models.URLField(blank=True)
    developer = models.ForeignKey('Developer', on_delete=models.CASCADE)
    icon = models.ImageField(upload_to=upload_dir)

    def ownedByDev(self, developer):
        return self.developer == developer

    def get_latest_version(self):
        """ Returns the latest version object """
        latest = Version.objects.get(
          application_id=self.id, latest_version=True)
        return latest
\end{minted}

Sono stati omessi gli import statement ed alcune funzioni che avrebbero
appesantito la trattazione. Gli attributi della classe Application sono tutti
creati sfruttando i "Field" di Django; il loro funzionamento è stato già
brevemente visionato nel capitolo precedente, ma se si volesse approfondire si
potrà farlo consultando la documentazione \cite{software:django:fields}.

L'unico attributo che merita attenzione particolare è "icon" che viene
inizializzato con un "ImageField" al quale è possibile specificare, tramite
keyword argument, una funzione di callback da invocare quando sarà necessario
salvare il file: essa verrà utilizzata per ottenere il path dove salvare
le immagini. Questa funzione è definita all'inizio del file e ritorna
semplicemente una stringa creata dinamicamente in base al nome dell'applicazione
ed al nome del file; il risultato finale sarà qualcosa del tipo
"applications/Firefox/icons/icon.png". La cartella creata, verrà utilizzata
per salvare tutti gli altri file relativi all'app, in modo da mantenere
una gerarchia pulita all'interno del file system.

Per implementare il salvataggio dell'applicazione, abbiamo varie possibilità.
La più immediata è realizzare una funzione che esegua le seguenti
operazioni:

\begin{itemize}
  \item Catturare i parametri passati tramite richiesta HTTP POST
  \item Effettuare la validazione di tali parametri, coerentemente a quanto
  presente nel file Applications.py
  \item Creare l'oggetto Application inizializzato con i valori del punto
  precedente
  \item Salvare l'oggetto Application nel database
  \item Reindirizzare l'utente verso una pagina di successo
  \item Se qualche dato inserito nella form HTML non risulta valido,
  reindirizzare l'utente verso la pagina di upload, ma con dei messaggi di
  errore personalizzati per ogni Field invalido
\end{itemize}

Tutte queste operazioni possono essere eseguite facilmente tramite l'API di
Django, ma non è questa la scelta migliore. In Softhub si è fatto utilizzo
delle generic views di Django che permettono di utilizzare codice generico
presente all'interno di Django, che realizza già tutti i punti espsoti in
precedenza! Grazie al principio di Inversion of Control potremo affidare lavoro
a Django e prendere il controllo del flusso dell'applicazione, solo quando
ne avremo bisogno, per alterare il comportamento di default.

Per sfruttare questa caratteristica, estendiamo la classe \textbf{CreateView}, e
per personalizzarne il comportamento, faremo l'overriding di alcuni metodi:

\begin{minted}[frame=lines, label=views/ApplicationUpload.py, linenos]{python}
class ApplicationUpload(CreateView):
    success_url = reverse_lazy('softhub:index')
    template_name = 'softhub/application_form/application_form.html'
    form_class = ApplicationForm

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        if self.request.user.isDeveloper():
            return super().dispatch(request, *args, **kwargs)  # Ok
        else:
            raise PermissionDenied()

\end{minted}

Dalla documentazione leggiamo che: \textit{
"CreateView is a view that displays a form for creating an object,
redisplaying the form with validation errors (if there are any) and saving
the object."}. Dunque è esattamente quello che fa al caso nostro!
Al suo interno dovremo avere l'accortezza di inizializzare vari attributi:

\begin{itemize}

  \item "success\_url" deve contenere l'URL da richiamare se la creazione
  avviene con successo

  \item "template\_name" deve contenere il nome del file template

  \item "form\_class" deve contenere la classe Form da utilizzare

\end{itemize}

I primi due punti sembrano scontati, il terzo un po' meno. Dobbiamo creare
una classe form ? Perché dovremmo farlo, se possiamo specificare una form
direttamente all'interno del template ? In realtà, realizzando una classe
specifica per la form, Django andrà ad utilizzarla per creare il codice
HTML associato alla pagina di upload. Ci fornirà questo contenuto all'interno
del template, tramite la variabile contestuale "form". Di fatto per ottenere
il risultato in figura \ref{softhub:form}, sarà sufficiente il codice contenuto
in "application\_form.html".

\begin{minted}[frame=lines, label=application\_form.html, linenos]{html}
{% extends 'softhub/base.html' %}

{% block content %}
  <h3>Application creation</h3>
  <form action={% url 'softhub:app_upload' %}
        method="post"
        enctype="multipart/form-data">
    {% csrf_token %}

    {% load crispy_forms_tags %}
    {{ form|crispy }}

    <input type="submit" value="Submit" class="btn btn-default">
  </form>
{% endblock %}
\end{minted}

Quanto realizzato fin'ora è estremamente potente: quando avremo bisogno di
aggiungere un attributo alla classe Application, tutto quello che dovremo fare è
definire l'attributo tramite un Field di Django. Tutto il resto del codice che
abbiamo scritto, si prenderà carico di aggiungere del codice HTML alla form, di
aggiungere la validazione, di salvare l'oggetto (compreso di nuovo attributo!) e
di gestire i messaggi di errore.

\begin{figure}
  \centering
  \includegraphics[width=1.0\textwidth]{img/softhub/form}
  \caption{Form HTML creata sfruttando la classe ModelForm di Django}
  \label{softhub:form}
\end{figure}

Da notare come nel codice di template, tutta la form viene portata in output
dalla riga \mintinline{html}{{{ form|crispy }}}. Tramite questo comando, il
template engine, è in grado di analizzare il contenuto della form, arricchirlo
tramite classi CSS di Bootstrap \footnote{Bootstrap è supportato dal pacchetto
crispy \url{https://github.com/django-crispy-forms/django-crispy-forms}} e
renderizzarne il contenuto; in caso di errori, si occuperà di
assegnare la corretta classe CSS, mostrando una casella rossa con il relativo
messaggio di errore.

Andiamo ora ad analizzare come deve essere strutturato il codice della classe
Form:

\begin{minted}[frame=lines, label=views/ApplicationForm.py, linenos]{python}
class ApplicationForm(ModelForm):
    def __init__(self, user_id=None, *args, **kwargs):
        self.user_id = user_id
        super().__init__(*args, **kwargs)

    class Meta:
        model = Application
        exclude = ['developer']

    def save(self, commit=True):
        app = super().save(commit=False)
        dev = Developer.objects.get(user__id=self.user_id)
        app.developer = dev
        app.save()
        return app
\end{minted}

Anche qui Django ci fornisce una classe generica da estendere,
\textbf{ModelForm}; essa si occupa di fare l'introspezione sull'oggetto
Application, riuscendo ad individuare quali e quanti campi della form generare,
e con quali validatori. Da notare che la validazione sarà fatta sia client side
(nei limiti di quanto si possa fare con HTML5 e dunque non verranno creati
controlli dinamici in JavaScript), che server side. Dunque la classe
ApplicationForm estende ModelForm, ritoccando leggermente il funzionamento della
classe padre per soddisfare le nostre esigenze: il costruttore salverà
"user\_id" come attributo di classe, così da poter sfruttare questa informazione
per associare lo sviluppatore attualmente in sessione, all'applicazione creata.
Alla riga 6 si utilizza la meta-classe per specificare la classe model sulla
quale eseguire l'introspezione ed inoltre è possibile specificare alcuni campi
da escludere dalla form: "developer" infatti non deve essere inserito
manualmente, ma dovrà essere rilevato automaticamente dall'applicazione in base
all'utente attualmente in sessione.
Per realizzare quanto appena descritto, è sufficiente eseguire l'override del
metodo "save", come visto nelle righe 10-15.

Qui potrebbe sorgere un dubbio, in quanto, all'inizio di questa sezione, avevamo
affermato che gli sviluppatori sarebbero stati gli unici attori in grado di
eseguire l'upload di applicazioni. Perché nella classe form non eseguiamo alcun
controllo sul tipo di utente? La risposta è semplice: non è responsabilità della
classe form occuparsi di questo. La responsabilità è, invece, della classe view
che ha il compito di prendere una richiesta HTTP e ritornare una risposta HTTP
adatta. Si può notare infatti che nelle righe 6-11 della classe
"ApplicationUpload.py" abbiamo eseguito l'override del metodo "dispatch", in modo
tale che solo utenti con permessi di sviluppatore possano accedere alla pagina
di upload; se qualcun altro tentasse di accedere all'URL, si ritroverebbe di
fronte ad una risposta HTTP con stato Permission Denied.

\paragraph{Visualizzazione pagina dei dettagli}
Ora che abbiamo realizzato la funzionalità di salvataggio, vogliamo visualizzare
una pagina dell'applicazione, contenente tutti i dettagli. Anche per questo
scenario, Django ha una generic-view apposita, chiamata \textbf{DetailView}.
Per ognuna di queste generic-view il processo per utilizzarle è il solito:
si creata una classe che eredita da quella generica e ne estende il
funzionamento.


\begin{minted}[frame=lines, label=views/ApplicationDetail.py, linenos]{python}
class ApplicationDetail(DetailView):
    model = Application
    template_name = 'softhub/application_detail/application_detail.html'

    def get_context_data(self, **kwargs):
        context = super(ApplicationDetail, self).get_context_data(**kwargs)
        app = self.get_object()
        context['versions'] = Version.objects.filter(application_id=app.id)

        return context
\end{minted}

Il metodo "get\_context\_data" si occupa di popolare le variabili di contesto
che verranno fornite al template engine. Di default, una DetailView, crea una
variabile "application" da utilizzare nel template: per farlo utilizza il
nome della classe model in minuscolo. Dato che nella pagina dei dettagli,
vogliamo visualizzare anche altre informazioni, aggiorniamo la lista delle
variabili di contesto aggiungendo le versioni relative all'applicazione
visualizzata.
Attraverso una composizione di template (vedi qui per l'implementazione completa
) abbiamo completato il caso d'uso!

\begin{figure}[H]
  \centering
  \includegraphics[width=1.0\textwidth]{img/softhub/detailview}
  \caption{Il risultato della classe view ApplicationDetail.py}
  \label{softhub:detailview}
\end{figure}
