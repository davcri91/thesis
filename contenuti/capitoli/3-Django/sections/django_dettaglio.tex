\section{Django in dettaglio}
  Qui ci immergeremo nell'imponente architettura di Django, dove centinaia di
  moduli Python collaborano tra di loro per offrire un'ambiente di sviluppo
  incredibilmente veloce, sicuro e ricco di funzionalità.

  \subsection{Installazione}
  Abbiamo visto nei precedenti capitoli, come Python sia fornito di un ecosistema
  ricco per amministrare i propri progetti software: ci fornisce un package
  manager e un virtualizzatore di ambiente con i quali poter amministrare i nostri
  progetti.
  Per installare Django sfruttiamo dunque \textbf{pip}:

  \begin{figure}
    \centering
    \includegraphics[width=0.7\textwidth]{img/django/pip_install_django}
    \caption{Installazione Django tramite pip}
    \label{django:pip_install}
  \end{figure}

  il package manager si occuperà di scaricare l'ultima versione stabile di
  Django dal repository PyPI e di installarla nel corretto path di sistema. Nel
  caso in cui fosse necessario installare una versione specifica di un
  pacchetto, sarebbe sufficiente specificarla con la sintassi
  \mintinline{shell}{pip install django==1.8}.

  A questo punto sarà disponibile \mintinline{c}{django-admin}, un comando
  che si occuperà di generare il progetto; eseguiamo quindi
  \mintinline{c}{django-admin startproject myapp}, dove
  \textit{myapp} è il nome che abbiamo scelto per il nostro progetto.
  I file e le cartelle automaticamente generate saranno le seguenti:

  \begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{img/django/startproject_shad}
    \caption{Struttura del progetto}
    \label{django:startproject}
  \end{figure}

  \begin{itemize}

  \item la cartella \textit{myapp} esterna è semplicemente un
  contenitore per i file di progetto, il suo nome può essere cambiato in
  qualsiasi momento senza impattare sul funzionamento dell'app.

  \item la cartella \textit{myapp} interna è invece un package Python contenente
  la nostra applicazione.

  \item \textbf{manage.py} è un tool di amministrazione del progetto Django,
  il vero e proprio centro di controllo; lo useremo costantemente durante lo
  sviluppo per migrations, DB, esecuzione del server ed anche per comandi
  personalizzati. Come si può intuire dall'estensione del file, è scritto in
  Python.

  \item \textbf{myapp/\_\_init\_\_.py} è un file Python vuoto che serve a segnalare
  all'interpete che la cartella che lo contiene deve essere valutata come un
  package Python con moduli importabili. \footnote{Per approfondire sulla
  struttura dei package Python, si rimanda alla documentazione ufficiale:
  \url{https://docs.python.org/3/tutorial/modules.html\#tut-packages}}

  \item \textbf{mysite/settings.py} è il file di configurazione per il progetto,
  contiene informazioni fondamentali per il corretto funzionamento dell'app:
  elenco dei moduli di terze parti da attivare, percorso al file di mapping,
  configurazione di template engine, database, timezone, motori di caching e
  così via.

  \item \textbf{mysite/urls.py} è il file di di mapping delle URL; di default
  il file di configurazione autogenerato, punta a questo file.

  \item \textbf{mysite/wsgi.py} è l'entry-point per qualsiasi web server
  compatibile con lo standard WSGI: si tratta di un acronimo che sta per
  \textit{Web Server Gateway Interface}, ovvero una specifica che descrive come
  i web server comunichino con le applicazioni web Python. Come tutti gli
  altri argomenti critici riguardanti questo linguaggio, è documentato
  ufficialmente in un PEP, il numero 3333 in questo caso
  \cite{software:python:pep3333}.

  \end{itemize}

  Ora che abbiamo il codice di base, vogliamo eseguirlo per vedere se tutto
  è andato a buon fine, per fare ciò dovremmo assicurarci di avere un ambiente
  configurato, ma vedremo come Django gli sviluppatori del framework ci abbiano
  fornito uno strumento pronto out-of-the-box, che necessita di una configurazione
  minima.

  \subsection{Ambiente di sviluppo}

  Chi avrà lavorato su applicazioni web in passato, avrà notato che non sono
  stati installati ancora alcuni componenti fondamentali per il funzionamento:
  il server web, il database server ed un tool per amministrare il database. La
  preparazione dell'ambiente di lavoro è tediosa, oltre che essere sempre
  accompagnata da problemi di vario tipo; sia il server web che il
  database richiedono infatti un lavoro di amministrazione che può portare via
  diverso tempo di lavoro. Come è possibile che Django abbia come vanto la
  caratteristica di \textit{ridiculously fast}? Questo si spiega mostrando tutti
  i sistemi che abbiamo già pronti per l'utilizzo; l'applicazione web appena
  creata è già eseguibile ed in grado di rispondere a richieste HTTP con una
  pagina di default; tutto questo senza scrivere alcuna riga di codice e,
  sopratutto, senza dover installare e configurare alcun software addizionale.
  Lavorando un po' con il framework scopriremo ulteriori funzionalità che
  l'applicazione "myapp" di esempio comprende: database, migrations,
  autenticazione, template system, supporto all'internazionalizzazione,
  middleware per la sicurezza ed una sezione per gli amministratori.
  Dunque andiamo ad analizzare singolarmente i sistemi a disposizione, mostrando anche
  quanta cura sia stata dedicata nella realizzazione di questi strumenti:

  \subsubsection{Server Web} Elemento fondamentale per eseguire la nostra
  applicazione; normalmente si andrebbe ad installare un software tra
  \textit{apache} e \textit{nginx}, per poi andare ad eseguire configurazione di
  directory pubbliche, moduli per il linguaggio utilizzato e così via. Tutte
  queste operazioni sono delicate e possono rallentare lo sviluppo; in grossi
  progetti software, ci sono team specializzati nel rilascio che si occupano di
  effettuare il deploy sulle macchine di produzione.

  Django fornisce un server web integrato che non necessita di
  configurazioni! Per accedere a questo server, è sufficiente eseguire il comando
  \mintinline{shell}{python manage.py runserver} dalla cartella principale del
  progetto:

  \begin{figure}
    \centering
    \includegraphics[width=0.9\textwidth]{img/django/runserver1}
    \caption{Esecuzione del server web integrato}
    \label{django:server:run1}
  \end{figure}

  L'output riporta diversi messaggi informativi, uno di essi sembra anche
  preoccupante; da bravi programmatori superficiali, ignoriamo tutto (no, in realtà
  affronteremo in dettaglio questi argomenti, poco più avanti) e proviamo ad
  accedere all'applicazione web, visitando l'indirizzo di \textit{localhost}
  riportato sul terminale \footnote{Su sistemi operativi GNU\textbackslash
  Linux, è possibile fare CTRL+click per far aprire l'indirizzo sul browser web
  di default; questo è possibile grazie all'applicazione del terminale che
  risulta più funzionale rispetto al semplice terminale di Windows.}. Per farlo
  non ho utilizzato un browser, ma l'utility open source \textbf{httpie}
  \footnote{\url{https://github.com/jakubroztocil/httpie}} che permette di
  analizzare semplicemente le informazioni scambiate nelle comunicazioni HTTP.
  Tra l'altro questa utility si rivela molto utile quando l'applicazione web
  viene installata su un calcolatore con sistema operativo server, dove l'unico
  modo per testare il funzionamento della web-app è generare una richiesta HTTP da
  riga di comando: solitamente su questi sistemi operativi non si ha a disposizione
  un'interfaccia grafica con cui poter avviare un browser moderno.

  \begin{figure}
    \centering
    \includegraphics[width=1.0\textwidth]{img/django/runserver_http}
    \caption{Prima chiamata HTTP alla nostra applicazione}
    \label{django:server:http}
  \end{figure}
  % TODO taglia screenshot ?

  Innanzituto osserviamo la metà destra dello screenshot: possiamo verificare
  che l'applicazione web è attiva e funzionante in quanto la risposta del server è
  stata positiva: abbiamo ottenuto infatti un messaggio HTTP con codice 200, che
  corrisponde ad una comunicazione avvenuta senza errori. Una lista
  degli status code HTTP è curata dalla IANA ed è consultabile sul loro sito web
  ufficiale \cite{software:http:iana_status_codes}.

  Torniamo ora ad analizzare l'output minaccioso che abbiamo incontrato; esso è
  diviso in tre parti.

  La prima offre un report sui \textbf{system check}: essi sono dei \textit{test
  statici} \footnote{L'analisi statica di un programma si riferisce all'analisi
  del codice sorgente. L'analisi dinamica si occupa della verifica di
  determinate condizioni sul codice in esecuzione, eseguendo dei test a
  run-time.} effettuati per validare l'applicazione. Servono per rilevare
  problemi comuni e fornire suggerimenti su come risolverli. Django fornisce un
  insieme di check integrati, ma volendo è possibile scriverne di propri
  \footnote{\url{https://docs.djangoproject.com/en/1.11/topics/checks/\#writing-your-own-checks}}.
  Tra i check che vengono eseguiti di default, troviamo i \textit{Security
  Checks} che aiutano ad evitare dimenticanze o errori grossolani che
  comprometterebbero la sicurezza dell'applicazione come l'attivazione del
  middleware di protezione da attacchi CSRF, oppure come la corretta
  configurazione dei certificati \textit{SSL} (Secure Socket Layer). Bisogna
  fare attenzione in quanto questi check non rendono automaticamente sicura
  l'applicazione, né proteggono da intrusioni. Una lunghissima lista completa di
  tutti i check eseguiti è disponibile, come al solito, sulla documentazione
  ufficiale \footnote{\url{https://docs.djangoproject.com/en/1.11/ref/checks/}}.
  Prima di chiudere questo argomento, vorrei evidenziare anche la presenza dei
  \textbf{core system check} i quali verificano la presenza di potenziali
  problemi di backward compatibilty, che potrebbero avvenire in caso di un
  upgrade del framework: ricordate quando abbiamo narrato la storia di
  successo dell'upgrade dell'infrastruttura di Instagram
  \ref{python:instagram_success_story} ? Avendo a disposizione strumenti del
  genere, in grado di supportare ogni passo della transizione, prendere una tale
  decisione risulta quasi triviale.

  % TODO continua
  La seconda parte dell'output, mostra un messaggio di errore, evidenziato in
  rosso: il sistema ci avverte che ci sono delle \textbf{migrations} salvate che
  non sono state eseguite; il database infatti non è sincronizzato con le
  migrations. Queste ultime sono standard di qualsiasi applicazione Django e
  servono per supportare di default l'autenticazione, le sessioni e
  l'interfaccia per l'amministratore di sistema. Più avanti vedremo come
  eseguirle per aggiornare il database.

  La terza ed ultima parte riporta lo stato attuale dell'applicazione con alcune
  informazioni come il file di configurazione utilizzato, la versione del
  framework e l'indirizzo IP; nelle righe sottostanti verrà riportato un utile
  log di tutte le richieste HTTP ricevute.

  Ora che abbiamo verificato che l'applicazione venga eseguita senza errori,
  procediamo ad esplorare l'ambiente di sviluppo.

  \subsubsection{Database engine e migrations} Come database, l'installazione di
  Django utilizza \textbf{sqlite} di default, ma il framework è altamente
  flessibile supportando svariati engine diffusi sul mercato come MySQL,
  PostgreSQL ed Oracle. Sqlite è stato scelto di default in quanto è un semplice
  e comprovato DBMS server-less, risulta infatti il database engine più
  utilizzato nel mondo \cite{software:sqlite:most_deployed}; esso è in grado di
  funzionare autonomamente e senza particolari accorgimenti. L'unica
  informazione di cui ha bisogno è il nome ed il path del file che verrà
  utilizzato per salvare il database; questa configurazione è già presente nel
  file \mintinline{c}{settings.py} auto-generato, basta osservare le righe
  76-81:

  \begin{figure}[H]
    \centering
    \includegraphics[width=0.9\textwidth]{img/django/settings-database}
    \caption{Configurazione di default del database}
    \label{django:database:config}
  \end{figure}

  Come già pre-annunciato, il file di configurazione è scritto in Python,
  infatti possiamo riconoscere una variabile "DATABASES" contenente un
  dizionario Python con tutte le configurazioni relative ai databases con cui
  l'applicazione si interfacciarà; La riga 79 potrebbe sembrare ostica per chi
  non ha mai utilizzato la funzione \mintinline{c}{os.path.join} di Python, ma
  il suo compito è semplicemente quello di creare un path in maniera
  indipendente dal sistema operativo \footnote{la funzione è infatti in grado di
  utilizzare il giusto separatore su diversi sistemi operativi: lo slash su
  sistemi Windows, ed il backslash su sistemi GNU\textbackslash Linux ed OS X
  \url{https://docs.python.org/3/library/os.path.html\#os.path.join}}. Il nome
  del database che ci aspettiamo, sarà dunque "db.sqlite3".

  Per creare il DB non utilizzeremo comandi SQL, ma sfrutteremo il sistema di
  migrations: in precedenza abbiamo ricevuto un promemoria su delle migrations
  non applicate, ora è arrivato il momento di eseguirle. Utiliziamo il comando
  \mintinline{shell}{python manage.py migrate}. Il framework leggerà il file
  \textit{settings.py} per poi tradurre le migrations in comandi SQL adatti a
  creare lo schema DB con la tecnologia specificata: in questo caso creerà un
  file per il database sqlite3.

  \begin{figure}[H]
    \centering
    \includegraphics[width=0.75\textwidth]{img/django/migrate1}
    \caption{Esecuzione delle migrations di default}
    \label{django:database:migrate}
  \end{figure}

  \subsubsection{Template Engine} Il template engine, così come il database, non è
  necessario per eseguire la prima scarna versione dell'applicazione, ma è uno
  strumento necessario già dall'implementazione del primo caso d'uso. Come tutte
  le altre componenti tipiche che si rendono necessarie allo sviluppo di una
  moderna web-app, Django è dotato di un template engine pre-configurato: si
  tratta di un sistema sviluppato appositamente per il framework stesso e si
  basa su una sintassi chiamata \textbf{DTL}, il \textit{Django Template
  Language}. Per chi volesse utilizzare un template engine di terze parti, sarà
  sempre possibile farlo, creando una classe adapter (che permetta il corretto
  scambio di informazioni tra il layer View ed il layer di Template) e
  configurando il file di configurazione
  \footnote{\url{https://docs.djangoproject.com/en/1.11/topics/templates/\#custom-backends}}.
  All'interno di questo file possiamo anche definire in quale cartella andremo
  ad inserire i nostri template; visto che più avanti avremo bisogno di caricare
  dei template, approfittiamo e configuriamo l'opzione "DIRS" nel seguente modo:

\begin{figure}[H]
  \centering
  \includegraphics[width=0.9\textwidth]{img/django/template_dir_config}
  \caption{Configurazione di default del template engine}
  \label{django:template:config1}
\end{figure}

% \begin{figure}[H]
%   \centering
%   \includegraphics[width=0.9\textwidth]{img/django/template-conf}
%   \caption{Configurazione di default del template engine}
%   \label{django:template:config}
% \end{figure}

  % \paragraph{} Con quest'ultima parte concludiamo  L'unico passo che abbiamo
  % dovuto compiere per avere un'infrastruttura completa ed eseguibile è stato un
  % semplice comando da terminale: \textit{"django-admin startproject myapp"} and
  % you're ready to rock!

  % TODO rivedi questa parte...
  \subsection{Funzionalità framework} Le funzionalità offerte da Django sono
  molteplici, alcune di esse verrano mostrate nel capitolo relativo
  all'applicazione sviluppata, attraverso esempi presi dal codice sorgente. Per
  le altre si rimanderà alla documentazione ufficiale. Qui andremo a vagliare
  alcune peculiarità di Django, che lo contraddistinguono dagli altri framework.

  \subsubsection{Django App}
  Django struttura i suoi progetti in moduli indipententi, chiamati \textbf{app}.
  Queste non sono altro che package Python che seguono delle convenzioni dettate
  dal framework; quando creiamo app con l'utility \textit{manage.py} essa ci
  fornirà già tutte le cartelle ed i file necessari per aderire alle
  convenzioni.
  Un progetto può essere costituito da una o più app che collaborano tra di loro;
  questo approccio sprona lo sviluppattore a suddividere il proprio progetto
  in moduli indipendenti e, dove possibile, riutilizzabili. Questa è
  una pratica comune, tant'è che sul repository PyPI è possibile trovare
  tantissime app Django riutilizzabili. Esiste anche il sito web
  \url{https://djangopackages.org/} che permette una ricerca
  semplificata e confinata proprio ai pacchetti relativi a Django.
  Attualmente sono disponibili circa 3500 pacchetti.
  Tra quelli più utilizzati troviamo:

  \begin{itemize}

    \item \textit{Django REST Framework}
    (\url{http://www.django-rest-framework.org/}): un modulo che permette di
    creare semplicemente delle API REST.

    \item \textit{Social Auth}
    \url{https://python-social-auth.readthedocs.io/en/latest/}: permette di
    aggiungere l'autenticazione alla propria applicazione tramite servizi
    esterni come Facebook, Google, Github, Twitter e così via.

    \item \textit{Django CMS} \url{https://github.com/divio/django-cms}: un vero
    e proprio Content Management System realizzato con Django.

  \end{itemize}

  A volte per estendere il funzionamento della propria applicazione Django,
  sarà sufficiente trovare ed includere la giusta \textit{app}.

  Riprendiamo l'esempio sviluppato in questo capitolo e creiamo un'altra
  app al suo interno, utilizzando
  \mintinline{shell}{python manage.py startapp appstore}, dove "appstore"
  è il nome della nuova app che vogliamo creare:

  \begin{figure}[H]
    \centering
    \includegraphics[width=0.4\textwidth]{img/django/appstore_app}
    \caption{Struttura della una nuova app chiamata "appstore"}
    \label{django:startapp:appstore}
  \end{figure}

  Ci sono diversi file che sono stati creati di default per comodità, ma
  avremo piena flessibilità nel caso in cui volessimo rinominarli o spostarli
  in cartelle separate. Per attivare l'applicazione appena creata, sarà
  necessario andare nel file di configurazione di progetto "settings.py" ed
  aggiungere alla lista INSTALLED\_APPS, la stringa
  \textit{'appstore.apps.AppstoreConfig'} che punta al file che
  convenzionalmente viene utilizzato da Django per riconoscere un'app.

  \subsubsection{Models e database API} Le classi di dominio, chiamate models
  nell'ambito di Django, sono la parte cruciale dell'applicazione, in quanto
  sono la descrizione software della realtà. In Django i models hanno un
  importante ruolo, rappresentano infatti la sorgente definitiva
  dell'informazione, in accordo al principio software DRY. Essi contengono gli
  attributi ed i metodi che definiscono quali dati sono salvati e come questi
  possono essere utilizzati. Per la persistenza dei dati, Django utilizza il
  pattern \textit{Active Record} un noto pattern architetturale il quale prevede
  che ogni model sia associato ad una singola tabella del database, ed ogni
  attributo del model abbia un corrispettivo campo nella tabella. Per
  realizzare una classe model a livello di codice, sarà sufficiente estendere,
  tramite ereditarietà, la classe \mintinline{python}{django.db.models.Model}.
  Una volta fatto ciò, Django genererà automaticamente, per ogni model, un'API
  tramite la quale potremo accedere alle funzionalità del database.

  Nella programmazione, gli esempi aiutano sempre a chiarificare i concetti,
  dunque prendiamo spunto da alcune classi implementate per SoftHub (in una
  versione semplificata).

\begin{minted}[frame=lines, linenos]{python}
from django.db import models

class Review(models.Model):
    text = models.TextField()
    rating = models.IntegerField()
    date = models.DateField(auto_now=True)
    application = models.ForeignKey('Application')

class Application(models.Model):
    name = models.CharField(max_length=200)
    website = models.URLField(blank=True)
\end{minted}

  Le classi sono estremamente semplici, ma daranno tutte le informazioni
  necessarie al framework per creare lo schema del database e la relativa API
  Python per interfacciarsi. Per dichiarare un attributo di classe, si dovranno
  utilizzare le funzioni \textit{Field} presenti nel modulo
  \mintinline{python}{models}: queste permetteranno al framework di capire come
  strutturare il database. Le funzioni "TextField" e "IntegerField" fanno
  proprio quello che il nome suggerisce. Gli attributi "text" e "rating", della
  classe "Review", conterranno rispettivamente il testo della recensione e la
  valutazione rappresentata con un numero intero. L'attributo "date" rappresenta
  la data di inserimento della recensione e Django ha un Field anche per questo.
  Il parametro "auto\_now", passato come keyword \footnote{Da notare come la
  possibilità di passare parametri tramite keyword sia una buona pratica Python,
  che migliora notevolmente la leggibilità. Avevamo accennato a questi vantaggi
  nel capitolo dedicato al codice Pythonic \ref{pythonic:keyword_arguments}.},
  serve per dire a Django che quando verrà creata un'istanza di Review,
  l'attributo "date" dovrà essere inizializzato con la data attuale. Alla riga
  11 possiamo notare come ci sia un "CharField" che si differenzia dal
  "TextField", per la possibilità di impostarne un limita massimo di caratteri.
  Alla riga 12, definiamo un attributo di tipo URL che è accompagnato da un
  parametro "blank=True": questo dice a Django che l'attributo non è
  obbligatorio e può anche risultare vuoto. Tutti i Field utilizzati portano con
  loro dei validatori che si occuperanno di verificare i campi prima di salvarli
  sul database. In caso in cui l'attributo URL contenga, per sbaglio, una URL
  invalida, l'oggetto non verrà salvato sul database e verrà lanciata
  un'eccezione. Questi validatori potranno essere utilizzati anche per validare
  le form collegate ai model, massimizzando i vantaggi portati dall'approccio
  DRY.

  Per attivare queste classi model, è necessario creare le migrations relative
  ed eseguirle. Utilizziamo quindi \mintinline{shell}{python manage.py makemigrations}
  seguito da \mintinline{shell}{python manage.py migrate}. Il primo comando
  analizzerà le classi appena scritte, generando le corrette migrations; il
  secondo le applicherà al nostro progetto.

  \begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{img/django/makemigrations_migrate}
    \caption{Creazione ed esecuzione delle migrations per le classi model}
    \label{django:makemigrations}
  \end{figure}

  A questo punto sarà possibile avere completo accesso all'API per il database;
  di seguito mostriamo come sia possibile testare queste funzionalità, in una
  shell interattiva con ambiente Django pre-caricato. Per accedere a questa
  shell, si utilizza la solita utility "manage.py", specificando il parametro
  "shell":

\begin{minted}[frame=lines, linenos]{python}
# Creazione oggetti
firefox = Application(name="Firefox", website="www.mozilla.org")
firefox.save() # l'oggetto viene salvato sul DB

review = Review(
    text = "Fantastico web browser",
    application = firefox,
    rating = 5
)
review.save() # la recensione viene salvata sul DB

# Recupero oggetti
fire = Application.objects.get(name="Firefox")
print(fire.name, fire.website)  # -> Firefox, www.mozilla.org

last_review = fire.review_set.last()
print(last_review.text)  # -> Fantastico web browser
\end{minted}

  Molto del codice mostrato è estremamente leggibile. La parte più ostica è
  rappresentata dalla presenza di un attributo "objects" associato alle classi
  model: esso è un \mintinline{python}{Manager}
  \footnote{\url{https://docs.djangoproject.com/en/1.11/topics/db/managers/\#django.db.models.Manager}}
  inserito da Django nella class model, per accedere alle funzionalità del
  database. Alla riga 16 notiamo l'attributo "review\_set", esso è un
  \mintinline{c}{RelatedManager}
  \footnote{\url{https://docs.djangoproject.com/en/1.11/ref/models/relations/}}
  che viene utilizzato per accedere agli oggetti che sono in relazione
  con l'istanza chiamante: in questo caso ogni Review ha una Foreign Key
  che punta ad un oggetto Applicaton. Tramite "fire.review\_set",
  troveremo l'insieme delle recensioni legate all'applicazione "fire". Da qui
  si utilizza il metodo "last()" per ottenere l'ultima recensione salvata
  sul database.

  Le funzionalità mostrate qui sono solo una piccolissima parte dell'immensa
  API auto-generata.
  Una completa guida sull'argomento è possibile consultare il seguente
  link: \url{https://docs.djangoproject.com/en/1.11/topics/db/queries/}.

  \subsubsection{URL routing e Views} Le view in Django hanno una responsabilità
  ben definita: esse prendono in ingresso una richiesta HTTP e restituiscono una
  risposta HTTP. In questo caso il framework sfrutta pienamente il suo paradigma
  ad oggetti, imponendo che il parametro in ingresso ad una view sia di tipo
  \mintinline{c}{HTTPRequest} e che il valore restituito sia una
  \mintinline{c}{HTTPResponse}. Entrambe queste classi sono definite all'interno
  del framework e possono essere importate accedendo al modulo
  \mintinline{c}{django.http}. Apriamo dunque il file \textit{urls.py} e
  modifichiamolo come segue:

\begin{minted}[frame=lines, linenos]{python}
from django.conf.urls import url
from django.contrib import admin
from django.http.response import HttpResponse

  def hello_view(request):
      return HttpResponse("Ciao")

  urlpatterns = [
      url(r'^admin/', admin.site.urls),
      url(r'^hello/', hello_view),
  ]
\end{minted}

  La variabile \textit{urlpatterns} è fondamentale, poiché sarà la variabile che
  Django utilizzerà per attivare le URL ed effettuare il dispatch delle
  richieste HTTP: tramite il metodo \mintinline{c}{url()} potremmo aggiungere
  una rotta, specificando l'URL, la funzione da eseguire ed anche altri
  parametri come il nome della rotta (non mostrato nell'esempio). La sintassi
  per l'URL è leggermente particolare e necessita di spiegazione per chi non è
  pratico del mondo Python: una stringa preceduta dal carattere "r" sta ad
  indicare una stringa \textit{raw}, dunque una stringa i cui caratteri speciali
  come "\textbackslash n" non sono interpretati come sequenze di escape: questo
  si rende necessario perché la funzione \mintinline{c}{url()} di Django vuole
  che le URL vengano definite come espressioni regolari
  \footnote{\url{https://it.wikipedia.org/wiki/Espressione\_regolare}} che sono solite
  contenere diversi caratteri speciali. Da notare che questo file urls.py
  dovrebbe avere un'unica responsabilità, ovvero quella di contenere il mapping
  delle rotte: quanto mostrato nell' esempio è una cattiva pratica in quanto
  abbiamo inserito al suo interno codice riguardante le view; purtroppo ciò si
  rende necessario negli esempi di questo capitolo per semplificare la
  trattazione, ma nell'applicazione SoftHub ogni componente sarà isolata
  scrupolosamente.

  Verifichiamo ora che il, poco, codice scritto funzioni correttamente.
  Utilizziamo di nuovo l'utility httpie:

  \begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{img/django/hello_view}
    \caption{Verifica del funzionamento della prima View}
    \label{django:view1}
  \end{figure}

  Ogni volta che vorremo aggiungere una View all'applicazione, dovremo compiere
  i passi appena visti: creare una View che gestisca la richiesta e risponda
  secondo le convenzioni adottate dal framework, aggiungere una rotta al file
  "urls.py" collegandola con la view appena creata.

  Ora vediamo come poter sfruttare il template system di Django, per essere in
  grado di restituire pagine con contenuto variabile. Per farlo sarebbe
  sufficiente importare il modulo \textit{django.template.loader} per poi
  sfruttarne l'API per caricare il file di template, assegnarli un contesto e
  poi renderizzarlo, inserendolo nella risposta HTTP. Non mostrerò questo
  esempio, in quanto Django ha un modo ancor più veloce e semplice per questo
  problema: durante la storia del framework, gli sviluppatori si sono accorti
  che molto codice delle view, veniva duplicato; praticamente per ogni richiesta
  si doveva recuperare un template, associargli un contesto (un insieme di
  variabili da inserire all'interno del codice HTML) e poi renderizzare l'output
  per inserirlo nella rispsota HTTP. Dal 2011, tutte queste operazioni sono
  state messe a fattor comune nelle \textbf{Class-Based-Views}, ovvero un
  insieme di classi View generiche altamente configurabili e riutilizzabili
  \footnote{\url{https://docs.djangoproject.com/en/1.11/topics/class-based-views/generic-display/}} .

  Vediamone subito un esempio, creando questo semplice template:

\begin{minted}[frame=lines, linenos, label=myapp/templates/hello.html]{html}
<html>
  <head>
    <meta charset="utf-8">
    <title> Pagina di benvenuto </title>
  </head>
  <body>
    Ciao! Oggi è il {% now 'd-m-y' %}
  </body>
</html>
\end{minted}

  Alla riga 7 è possibile notare una sintassi diversa dall'HTML, questa è
  proprio relativa al DTL. Tramite doppie parentesi graffe "{{ }}" è possibile
  accedere alle variaibli di contesto (assenti in questo caso), mentre con la
  sintassi "{% %}" è possibile accedere ai tag. In questo caso il tag "now"
  server per inserire un timestamp, ma è possibile trovare diversi tag di
  utilità, tra cui anche tag che implementano costrutti condizionali
  \footnote{\url{https://docs.djangoproject.com/en/1.11/ref/templates/builtins/}}.
  

Per sfruttare le generic view, dovremo semplicemente ereditare da esse,
andando a specificare il comportamento che vogliamo implementare, eseguendo
l'overriding di opportuni metodi della classe padre, oppure specificando alcune
variabili di classe. Nel caso della classe \textit{TemplateView}, basterà inserire
una variaible "template\_name" autoesplicativa:

\begin{minted}[frame=lines, linenos]{python}
# NOTA: alcuni import statement sono stati omessi per semplicità.
from django.views.generic import TemplateView

class helloTemplateView(TemplateView):
    template_name = "hello.html"

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^hello/', helloTemplateView.as_view()),
]
\end{minted}

  Avendo a disposizione le nozioni su Python introdotte nel capitolo dedicato,
  non dovrebbe essere difficile capire cosa accade. Le uniche particolarità si
  trova alla riga 5 dove viene specificata un attributo di classe contenente il
  nome del template da caricare (le cartelle dove Django andrà a cercare i
  tempalte sono specificate nel file di configurazione) e alla riga 9 dove il
  secondo parametro del metodo "url()" richiede un tipo callable; per ottenere
  ciò da una class-based-view, esiste il metodo \textit{as\_view()} che si
  occupa di soddisfare tale requisito.

  Proviamo ad accedere alla rotta "hello/", per verificare che sia tutto
  configurato correttamente:

  \begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{img/django/template_out}
    \caption{Verifica del funzionamento del template system}
    \label{django:template:hello1}
  \end{figure}


  % TODO ? No, forse è meglio farlo vedere con un esempio reale nel capitolo
  % dell'applicazione SoftHub
  % \subsection{Esempio completo di flusso di controllo}
  % TODO spiegare in dettaglio IoC
  % TODO Esempio di tipico flusso di controllo e di come le responsabilità software
  % vengano distribuite tra i componenti del framework


  % TODO admin-app Django fornisce anche un'applicazione di amministrazione che permette di
  % eseguire le operazioni \textit{CRUD} (Create, Read, Update, Delete) tramite un
  % account di amministratore.

  % TODO Activer Record Pattern per i model (+ DRY)
  % TODO template engine, generic class-based views, migrations, database
  % abstraction
