\section{Caratteristiche Django}

 Avendo gettato le basi del Python, possiamo finalmente andare alla scoperta del
 mondo framework, studiando Django sotto gli aspetti più importanti; non
 forniremo ovviamente una spiegazione approfondita di tutte le componenti, per
 le quali è sempre possibile fare riferimento all'ottima documentazione ufficiale
 \cite{software:django:doc}.

 Innanzitutto daremo un'occhiata alla sua storia, proseguendo con la filosofia
 di sviluppo che lo ha contraddistinto. Analizzeremo anche lo stato di  salute
 del progetto, per assicurarci di utilizzare una tecnologia che possa garantirci
 una struttura resistente e flessibile nel tempo. Concluderemo con la parte più
 tecnica del framework, mostrando i passi necessari per l'installazione, la
 configurazione ed andando ad esplorare la struttura del framework e la
 suddivisione di responsabilità al suo interno.

  \subsection{Storia}

  % TODO parlare delle origini ?

  Django è un framework web open-source, scritto in Python.
  Il progetto è stato rilasciato, con licenza BSD, nel luglio 2005. In quel
  periodo Python 3 non esisteva, ed il framework è stato costruito sulla versione
  2 del linguaggio. Per approfondire la storia del framework e dei suoi
  momenti salienti, analizziamo la seguente tabella, consultabile da wikipedia
  \footnote{\url{https://en.wikipedia.org/wiki/Django_(web_framework)}}:

  \begin{figure}[H]
    \includegraphics[width=1.0\textwidth]{img/django/history}
    \caption{Storia delle versioni di Django}
    \label{software:django:history}
  \end{figure}

  Nel 2013 è arrivata la prima versione di Django con il supporto a Python 3.
  La versione disponibile attualmente è la 1.11 e sarà l'ultima a supportare
  entrambe le versioni di Python.

  A fine anno è previsto il rilascio dell'attesissima versione 2.0 di Django, la
  quale porterà importanti novità tra cui l'abbandono del codice di supporto per
  Python2 che dovrebbe consentire di snellire tutta la codebase, migliorando
  allo stesso tempo le performance. Lo sviluppo del framework è curato dalla
  Django Software Foundation (DSF), supportata costantemente da altri decine di
  altri contributors e supporters. Tra questi ultimi troviamo grosse compagnie
  come Mozilla ed Instagram che utlizzano Django per i loro progetti ed hanno
  interesse diretto per un suo continuo miglioramento. Nel 2015, Mozilla ha
  finanziato il progetto Django con 150'000 \euro{} come parte del suo programma
  \textit{Mozilla Open Source Support}
  \cite{software:mozilla:open_source_support}; i fondi sono stati principalmente
  indirizzati allo sviluppo di \textbf{Channels}
  \footnote{https://github.com/django/channels/}, un progetto ufficiale Django
  che attualmente fornisce il supporto ai background tasks e, sopratutto, a
  nuovi protocolli di rete estremamente efficienti come \textit{WebSockets} ed
  \textit{HTTP2}.
  La facilità con cui gli sviluppatori siano riusciti ad aggiungere il supporto
  ai nuovi protocolli, mantenendo piena retrocompatibilità con le funzionalità
  di Django, è buon indizio di come l'applicazione sia ben progettata e di come
  il progetto sia in buona salute.

  \subsection{Descrizione filosofia} L'obiettivo primario di Django è quello di
  fornire allo sviluppatore un ambiente che gli consenta di creare applicazioni
  complesse, riducendo il più possibile i tempi necessari per passare dal
  concept ad una prima versione eseguibile.

  Per raggiungere questi obiettivi, gli sviluppatori hanno sfruttato
  completamente le peculiarità di Python, lasciandosi ispirare anche dai suoi
  principi zen. Questo versatile linguaggio è utilizzato non solo per definire
  il comportamento dell'applicazione, ma anche per i file di configurazione, per
  la creazione di script di gestione del progetto, per descrivere le migrations
  e per eseguire i test e la manutenzione. Come è stato detto in precedenza,
  Python è un vero e proprio coltellino svizzero, in grado di rendersi utile
  nelle più disparate occasioni.

  Grazie all'esperienza accumulata dagli sviluppatori del framework, Django
  è stato costruito seguendo principi di sviluppo consolidati: tra quelli
  che hanno influenzato maggiormente il design troviamo il pattern
  architetturale \textbf{MTV} ed il principio \textbf{DRY} \textit{Don't Repeat
  Yourself} che incoraggiano uno sviluppo rapido. Andiamo ad approfondire questi
  tre importanti concetti.

  Il primo è l'acronimo di \textit{Model-Template-View} un pattern ispirato
  profondamente al più conosciuto pattern \textit{Model-View-Controller}; gli
  sviluppatori di Django spiegano \cite{software:django:MVT} che, nella loro
  interpretazione di MVC, la parte View è responsabile di \textit{quali} dati
  vengano visualizzati ma non \textit{come}. La parte \textit{Template} si
  occupa invece di come i dati vengano presentati all'utente. Il controller,
  dunque, consiste nel framework stesso che è stato già strutturato per
  supportare i casi d'uso più disparati dello sviluppo web. La parte
  \textit{Model} è invece coerente con il classico modello MVC, rappresentando
  le classi di dominio dell'applicazione, ovvero quelle che modellano la realtà
  e vengono salvate tramite il sistema di persistenza.

  Lo sviluppo rapido è il risultato del riuso costante di codice già presente
  nel framework: come detto prima, la parte "controller" dell'applicazione sarà
  il framework che agirà sotto le nostre indicazioni. Per dettare il
  comportamento desiderato, si ricorrerà spesso al principio di
  \textbf{Inversion of Control}. Questo principio ribalta completamente il modo
  di intendere la programmazione; normalmente si è sempre in grado di gestire il
  flusso e la logica del software, richiamando librerie riusabili ove
  necessario. Con il principio di IoC, avviene il contrario: il framework ha il
  controllo del flusso logico e richiamerà alcune parti di codice (ad esempio
  un metodo delle classi model). Lo sviluppatore dovrà quindi capire il
  funzionamento del framework per andare ad "iniettare" il codice in sezioni
  che il framework invocherà. In questo modo si riesce a minimazzare il codice
  da scrivere da parte del programmatore, aumentando la leggibilità del codice,
  a patto di conoscere il funzionamento del framework e i principi di sviluppo
  software.

  % Se il concetto non è chiaro, vedremo un esempio di
  % codice più avanti.
  % TODO inserisci grafico di esempio per IoC

  Il principio DRY è definito nel seguente modo: \textit{"Ogni frammento di
  informazione deve avere una singola, non ambigua ed autoritaria
  rappresentazione nel sistema"}. Il suo scopo è quello di incoraggiare la
  progettazione accurata delle sorgenti di informazioni, per poi farvi
  riferimento da ogni parte dell'applicazione. Definendo un unico punto di
  verità \footnote{Per questo motivo è chiamato anche \textit{Single Point of
  Truth}} per le informazioni, evitando di frammentare o, peggio ancora,
  ridondare queste informazioni all'interno del software. Vedremo come Django fa
  uso di questa funzionalità, ad esempio, nelle classi models, permettendo ad
  altre componenti del framework (form, database) di sfruttare
  questa conoscenza per fornire potenti funzionalità.

  È possibile trovare una lista di tutte le filosofie di progettazione che
  vengono utilizate da Django nella documentazione ufficiale:
  \url{https://docs.djangoproject.com/en/1.11/misc/design-philosophies/}. Tra
  quelle che non sono state menzionate, è doveroso sottolineare che l'intera
  infrastruttura è sviluppata mantenendo un loose coupling e tight cohesion, due
  principi GRASP che consentono di avere componenti altamente intercambiabili
  tra di loro. Django infatti è costituito da vari strati che conoscono poco, se
  non nulla degli altri. Ad esempio il template system non sa nulla delle
  richieste web, come lo strato di persistenza non sa nulla riguardo com i dati
  verranno mostrati.

  Questa caratteristica è estremamente importante quando si
  considera la futura espansione del software realizzato: dato che sono numerose
  le tecnologie con cui Django può interfacciarsi, sia a livello client-side che
  server-side. Per uno sviluppatore è importante sapere
  di poter aggiungere o cambiare una componente senza incontrare problemi
  di compatibilità.
  Per fare esempi pratici, Django è integrabile con React, AngularJS e Vue.js,
  i tre principali framework client-side presenti attualmente sul mercato.
  Allo stesso modo è possbile utilizzare diversi sistemi di persistenza tra cui
  sqlite, MySQL, PostgreSQL ed altri.

  % Illustrare le caratteristiche principali: velocità di sviluppo, sicurezza e
  % versatilità • Descrivere la filosofia di Django descrivendo l’architettura
  % software MVC ed il principio DRY.

  % • Discutere la flessibilità del framework e l’adattabilità a diverse tecnologie
  % sia client-side (UI toolkit, plugin, librerie JS) che server-side (API REST,
  % database SQL, search engine, ...)
