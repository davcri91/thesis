\section{Analisi del linguaggio Python}

  % Offrire una breve panoramica di Python: "The programming language of the agile era",
  % eleganza del codice, facilità di comprensione, sviluppo rapido e semplice, do-
  % cumentazione estesa. Queste sono tutte caratteristiche in sinergia con la
  % filosofia Django

  % Illustrare la maturità dell’ecosistema: virtualenv, disponibilità di linter,
  % beautifier, documentation generator, package manager, centinaia di moduli di
  % utilità

  % Illustrare brevemente il passato importante del linguaggio e analizzare il
  % promettente futuro (es: TensorFlow sviluppato da Google per Machine Learning)

  Cominciamo ora a parlare di Python, linguaggio che ho scoperto grazie proprio
  alla ricerca di un framework web moderno, adatto sia allo sviluppo di SoftHub
  che come conoscenza tecnologica utile per il futuro lavorativo. Cercando
  in rete, Django è risultato uno dei progetti più consigliati, sopratutto
  per la sua espressività sintattica derivata dal Python: l'ideale per chi
  non ha esperienza con i framework di sviluppo e per chi vuole realizzare
  applicazioni manutenibili nel tempo, grazie all'eleganza e leggibilità del
  codice prodotto.

  La filosofia di Django mi ha subito attirato, per via degli slogan presenti

  nella pagina web ufficiale:
  \begin{center}
    \textit{"The web framework for perfectionists with deadlines."}
  \end{center}

  \begin{center}
    \textit{"Django makes it easier to build better Web apps more quickly and with
    less code."}
  \end{center}

  Con queste premesse, sono andato ad esplorare la documentazione,
  trovandola sorprendentemente semplice, dato che non conoscevo il linguaggio
  Python, e sufficientemente dettagliata.
  L'unico ostacolo era rappresentato, appunto, dalla mia inesperienza con il
  linguaggio; decisi di approfondire, cercando di capire se valesse veramente
  la pena studiare da zero una nuova tecnologia, dato che nella mia esperienza
  universitaria avevo già studiato linguaggi ampiamenti diffusi come C++, Java
  e PHP.

  \subsection{Studiare Python: la scelta giusta ?}
  Python è rinomato per essere uno dei linguaggi di programmazione più semplici
  sia per scrivere che per leggere codice sorgente. Nel mondo dello sviluppo software,
  è sempre saggio indagare a fondo e testare di persona, piuttosto che credere
  ciecamente nelle affermazioni di altri.
  Ciò che sono andato ad analizzare è la salute generale del
  linguaggio e, per farlo, mi sono posto domende del tipo:
  \begin{enumerate}
    \item è diffuso nella community di sviluppatori ?
    \item ha un buon supporto a livello di documentazione ?
    \item ha un ecosistema maturo in grado di supportare ogni aspetto dello sviluppo ?
    \item è utilizzato nelle aziende e nel mondo lavorativo ?
    \item ha un futuro brillante di fronte a sé ?
    % \item è in grado di offrirmi nuove opportunità rispetto altri linguaggi che già conosco ?
  \end{enumerate}

  La risposta a tutte queste domande è stata incredibilmente positiva,
  convincendomi ad investire tempo nello studio di questa importante tecnologia.
  Andiamo ad esplorare il mondo \textit{Pythonic}, sviscerando puntualmente le
  questioni appena sollevate.

  \subparagraph{1. Diffusione:} Python è un linguaggio di programmazione
  interpretato, ad alto livello ed orientato agli oggetti, ideato nel 1991
  dall'informatico Guido van Rossum. La sua filosofia di progettazione
  enfatizza la leggibilità del codice, e la sua sintassi permette di scrivere
  programmi con minori caratteri e linee di codice rispetto ad un programma
  equivalente in C++ o Java.

  È particolare il fatto che abbia attualmente due versioni stabili: la 2.x e la
  3.x \footnote{Il carattere "x" rappresenta la \textit{minor version} nello
  schema di versioning.} La prima è stata rilasciata nel 2000 e risulta molto
  diffusa poiché tantissime applicazioni e script sono stati sviluppati prima
  del 2008, data di rilascio di Python 3. Python 2 è tutt'oggi
  supportato, ma l'end of life è previsto per il 2020 ed è altamente consigliato
  usare Python 3 sia per motivi di performance che di sicurezza.

  Stiamo parlando dunque di una tecnologia con più di 25 anni, la quale ha
  raggiunto una sua maturità da ormai diverso tempo.
  Grazie alla sua semplicità e versatilità, è stato utilizzato in diversi
  campi applicativi, come riportato sul sito ufficiale
  \cite{software:python:success_stories} dove sono elencate una serie di storie di
  successo: chimica, biologia, economia, matematica, simulazioni scientifiche,
  ed ovviamente anche l'informatica ed ingegneria.

  Per dare un'idea della diffusione che ha raggiunto oggi questo linguaggio,
  riporto un estratto dell'episodio 111 di \textit{"Talk Python to Me"},
  un rinomato e prezioso podcast su Python tenuto da Michael Kennedy, il quale,
  al minuto 28:21, ci racconta di una sua interessante indagine:

  \begin{center}
    \textit{"I did a thing that covered, basically, the active GitHub
    repositories, and Python is number two for the language of most active GitHub
    repositories, non-trivial ones. JavaScript being ahead of it, but I think
    JavaScript's over-counted, right? Like, if you do Django, you're also a
    JavaScript developer. If you do Flask or Pyramid, you're a JavaScript
    developer. If you do Ruby on Rails, you're a JavaScript developer, right,
    like, you can't avoid JavaScript."}
  \end{center}

  Il lavoro che menziona è consultabile online
  \cite{software:github:most_active_programming_languages_2016} ed è
  sintetizzato nella figura \ref{github:programming_languages_2016}.

  \begin{figure}[H]
    \includegraphics[width=1.0\textwidth]{img/github_programming_languages_2016}
    \caption{Linguaggi di programmazione più utilizzati su Github nel 2016}
    \label{github:programming_languages_2016}
  \end{figure}

  Escludendo JavaScript che è, di fatto, presente in ogni applicazione web,
  Python risulta il linguaggio di programmazione più utilizzato su
  Github nell'ottobre 2016, con un trend che risulta addirittura crescente.

  Altra fonte, ancora più autorevole, che rafforza la tesi esposta, è un
  articolo della IEEE sui linguaggi di programmazione più diffusi nel 2016
  \cite{software:ieee:top_programming_languages_2016}.

  \begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{img/ieee_programming_languages_2016}
    \caption{Classifica globale dei linguaggi di programmazione più diffusi nel 2016}
    \label{ieee:programming_languages}
  \end{figure}

  La classifica globale tiene conto dell'importanza che il linguaggio ricopre in
  vari settori dell'informatica: web development, desktop app, mobile app
  e sistemi embedded. Il punteggio viene calcolato a partire da un insieme di
  statistiche prese da varie fonti: numero di ricerche di Google per quel
  linguaggio, numero di progetti Github attivi, numero di thread aperti su
  StackOverflow e Reddit. Python è al terzo posto della classfica globale, un
  risultato eccellente ma non è il più interessante: se si limita la classifica
  ai linguaggi di programmazione per il mondo web, \textbf{Python
  risulta il linguaggio di programmazione di maggior tendenza del 2016}
  \footnote{Impostando la modalità "Trending" per il ranking.}.

  \subparagraph{2. Documentazione:} il sito ufficiale di Python fornisce
  un'ottima documentazione ufficiale \cite{software:python:official_docs},
  caratteristica che lo fa risaltare rispetto a linguaggi come Java, C++ e
  JavaScript, il solo PHP riesce ad essere al pari sotto questo punto di vista.
  C'è anche da dire che gli altri linguaggi hanno comunque una grossa copertura
  di documentazione grazie a decine di libri ed esempi online.

  Caratteristica unica del linguaggio Python è la presenza di \textbf{PEP} (
  \textit{Python Enhancement Proposal}), un progetto ufficiale che mira a
  documentare in dettaglio ogni parte dello sviluppo del linguaggio. Tutte
  queste informazioni sono accessibili per qualsiasi sviluppatore dal sito
  ufficiale; si possono trovare relazioni tecniche sulle funzionalità,
  sulla progettazione del linguaggio e su standard e protocolli.
  La definizione stessa è documentata nel PEP\#1 \cite{software:python:pep1}
  creato il 13-06-2000:

  \begin{center}
    \textit{PEP stands for Python Enhancement Proposal. A PEP is a design document
    providing information to the Python community, or describing a new feature for
    Python or its processes or environment. The PEP should provide a concise
    technical specification of the feature and a rationale for the feature.
    We intend PEPs to be the primary mechanisms for proposing major new features,
    for collecting community input on an issue, and for documenting the design
    decisions that have gone into Python. The PEP author is responsible for
    building consensus within the community and documenting dissenting opinions.}
  \end{center}

  Ci troviamo di fronte ad un linguaggio così semplice e ben documentato,
  da essere in grado di portare benefici potenzialmente a qualsiasi
  professionista, indipendentemente dal livello di esperienza e dal tipo di
  lavoro. Grazie a questa sua diffusione radicale, la community creatasi attorno
  a questo linguaggio è continuo movimento, portando alla creazione di svariate
  librerie, strumenti di supporto, tutorial, podcast e corsi fruibili
  gratuitamente online.

  \subparagraph{3. Ecosistema:} quando si valuta un linguaggio, non bisogna
  limitarsi ad analizzare il suo funzionamento; bisogna assicurarsi che attorno
  al linguaggio sia presente un insieme di tool maturi e pronti per la
  produzione.
  L'ecosistema Python, a tal proposito, è risultato uno dei migliori in cui
  mi sia imbattuto; ciò che intendo per "ecosistema" è l'insieme di tutti quei
  progetti software che nascono per supportare ed agevolare il workflow di
  realizzazione di un progetto basato su quel linguaggio specifico.

  Innanzitutto Python è pre-installato su sistemi operativi GNU\textbackslash
  Linux e OS X, mentre su sistemi operativi Windows viene spesso installato come
  dipendenza di altri programmi. È quindi immediato avere un ambiente pronto da
  utilizzare, e risulta altrettanto immediato essere efficienti con questo
  linguaggio grazie al numero considerevole di librerie ufficiali incluse nel
  linguaggio stesso, tra cui troviamo:

  \begin{itemize}
    \item \mintinline{python}{datetime}:
    modulo che permette di gestire date, fusi orari, e calcolare semplicemente
    differenze temporali tra due oggetti di tipo datetime.
    \item \mintinline{c}{sqlite3} permette di interfacciarsi a database di tipo sqlite3
    \item \mintinline{c}{email} supporto per parsing, manipolazione e
    generazione di messaggi email
    \item HTTP (client, server)
    \item encoder e decoder XML, JSON, HTML
    \item \mintinline{c}{sched} scheduler di eventi generico
    \item \mintinline{c}{argparse} per effettuare il parse delle opzioni ed argomenti passati
    via command-line

  \end{itemize}

  Per avere una panoramica completa su tutte quelle disponibili è possibile
  consultare la documentazione ufficiale al seguente indirizzo
  \url{https://docs.python.org/3/py-modindex.html}. Nonostante queste siano in
  grado di coprire praticamente la maggior parte delle problematiche comuni,
  Python offre una spaventosa scelta di librerie. Queste vengono raccolte in un
  repository ufficiale, chiamato \textbf{Python Package Index}, abbreviato
  spesso con PyPI. Per capire quanto questo repository sia utilizzato,
  calcoliamo il ritmo di caricamento di nuovi pacchetti; per farlo, sfruttiamo
  proprio la libreria \textit{datetime} che ho menzionato in precedenza e
  calcoliamo quanti giorni sono  passati dalla nascita del linguaggio:

\begin{minted}[frame=lines, linenos]{python}
from datetime import date

python_release = date(1991, 2, 20)  # 2 February 1991
today = date.today()  # 23 June 2017
delta = today - python_release

print(delta.days)         # 9620 days
print(delta.days / 365)   # 26.36 years

pypi_packets = 110671
print(pypi_packets/delta.days)  # ~ 11.5
\end{minted}

  11 nuovi moduli al giorno, è la frequenza impressionante con cui PyPI
  viene aggiornato: esso infatto conta attualmente 110671 pacchetti di terze
  parti che, messi in relazione con i 9620 giorni di esistenza del linguaggio,
  fornisce la straordinaria cifra che fa comprendere quanto Python sia un
  linguaggio attivo ed in salute.
  Per interfacciarsi a PyPI si può usare l'interfaccia web accessibile da
  \url{https://pypi.python.org/pypi}, oppure \textbf{PIP}, il
  \textit{package manager} ufficiale fornito assieme alla distribuzione Python
  installata sul proprio sistema operativo.

  Grazie a quest'ultimo, installare i pacchetti è un gioco da ragazzi
  \footnote{per chi ha maturato un minimo di esperienza con i comandi da
  terminale, conoscenza che si è rivela sempre fondamentale nel mondo
  dell'informatica, sopratutto per l'efficacia degli strumenti disponibili}:

\begin{minted}[frame=lines, linenos]{shell}
pip install django
\end{minted}

  Sorprendentemente, l'ecosistema Python ha un ulteriore tool estremamente
  prezioso in grado di supportare gli sviluppatori, i
  \textbf{virtualenv}. Riportiamo la definizione presa dalla
  documentazione ufficiale:

  \begin{center}

    \textit{"virtualenv is a tool to create isolated Python environments.
    virtualenv creates a folder which contains all the necessary executables to
    use the packages that a Python project would need.""}

  \end{center}

  Il problema principale che affronta è quello della gestione delle dipendenze
  e delle loro versioni e, indirettamente, dei permessi all'interno di un
  progetto.
  Grazie a questi ambienti virtuali, è possibile tenere isolati tutti i software
  binari, con relativo versionamento, necessari per eseguire l'applicazione;
  in questo modo sarà estremamente semplice replicare
  l'ambiente su un altro calcolatore, minimazzando i tempi di set-up.
  I reali vantaggi di questo strumento si
  apprezzano quando si lavora all'interno di un team, dove risulta spesso
  complicato configurare ed installare correttamente un'applicazione su tutti
  i computer dei singoli sviluppatori. Questa è una delle situazioni più
  spiacevoli durante il workflow: si perde tempo per la manutenzione
  del software e del sistema, senza essere produttivi.
  Anche da questo punto di vista, Python si dimostra essere un linguaggio
  intelligente e funzionale: ha incontrato un problema comune dello sviluppo
  software e ha creato un tool specifico per risolverlo nel migliore dei modi.

  Sono in pochi i linguaggi a poter vantare questo livello di supporto ufficiale,
  infatti spesso gli altri ecosistemi risultano più frammentati,
  richiedendo ricerche e test approfonditi per trovare tool in grado di
  donare agilità allo sviluppo.
  Python emerge con forza grazie al suo ecosistema così coeso ed integrato da
  far ritenere questo linguaggio il coltellino svizzero del mondo software:
  Python risulta versatile, veloce, flessibile, potente ed eclettico, uno
  strumento che tutti gli sviluppatori moderni dovrebbero avere nel proprio
  repertorio.

  \subparagraph{4. Python nel mondo del lavoro:} Prima di buttarsi a capofitto
  nello studio di Python, è necessario assicurarsi che ci sia una reale
  richiesta di sviluppatori con queste competenze, andando a verificare quante e
  quali aziende nella storia abbiano puntato su questo tipo di tecnologia. Che
  senso avrebbe studiare un linguaggio eccellente, se non vi è richiesta
  reale nel mondo del lavoro ?

  Vediamo allora qualche esempio di utilizzo, prendendo spunto da quanto
  stanno facendo le grosse aziende al giorno d'oggi. Seguendo assiduamente il
  mondo del software engineering, ho letto spesso di famose aziende che
  investono su questa tecnologia, utilizzandola in produzione per svariati
  compiti come web-app, tool di scripting o semplicemente come linguaggio per
  le "coding-interview" che vengono effettuate durante i questionari di lavoro.

  \begin{figure}[H]
    \includegraphics[width=1.0\textwidth]{img/python-for-all}
    \caption{Applicazioni ed aziende famose che utilizzano Python}
    \label{python:for_all}
  \end{figure}

  Google è probabilmente l'azienda più innovativa ed influente degli ultimi anni, è
  quindi un ulteriore segnale positivo sapere che richieda  la conoscenza di un
  linguaggio a scelta tra C++, Java e Python  durante i questionari di lavoro
  \footnote{https://youtu.be/ko-KkSmp-Lk?t=1m56s}, oltre che incoraggiare allo
  studio di questi linguaggi anche nella sua "Technical Development Guide"
  \cite{software:google:career_requirements} rivolta agli studenti.

  \label{python:instagram_success_story}
  Altra argomentazione a favore di Python è legata ad un'applicazione di enorme
  successo, \textit{Instagram}; il loro stack server-side è composto dall'
  accoppiata Python più Django, permettendo loro di gestire il caricamento di 95
  milioni di foto al giorno da parte di 600 milioni di utenti registrati; la
  scalabilità non è assolutamente un problema per Python, a differenza di quanto
  l'opinione comune faccia pensare.

  Altra importante storia di successo legata a questa azienda, è la sua
  transizione alla nuova versione di Python, che è stata descritta in un
  articolo di \url{www.thenewstack.io} in cui viene riportata l'intervista a due
  software engineer che lavorano persso Instagram
  \cite{software:instagram:python3_transition}; fino ad Aprile 2016 la base di
  codice era costituita in maggior parte da Python 2. Il numero di utenti
  continuava a crescere copiosamente e il linguaggio, nella vecchia versione,
  non era più adatto alle esigenze prestazionali del servizio. Si arrivò al
  punto di dover scegliere se continuare con Python, oppure cambiare linguaggio
  per optare per qualcosa di più performante. La scelta fù quella di proseguire
  con Python, aggiornandolo alla nuova versione: gli sviluppatori hanno così
  potuto sfruttare l'esistenza, nell'ecosistema Python, di numerosi pacchetti
  creati dal team ufficiale appositamente per supportare tale transizione. Tra questi
  meritano una menzione \mintinline{python}{"six"} e
  \mintinline{python}{"2to3"}: il primo è un modulo che contiene funzioni e
  strutture dati compatibili con entrambe le versioni, in modo da non perdere
  nessuna funzionalità di livello applicativo, lasciando invariata l'esperienza
  utente; il secondo pacchetto fornisce un comando che prende in ingresso codice
  Python 2 e fornisce in output un codice Python 3 che tenta di replicarne il
  comportamento. Il file così ottenuto, permette di non riscrivere completamente
  il codice da zero, fornendo una base grezza da perfezionare a piacimento.

  Dunque, attraverso un attento percorso di 10 mesi, il team di Instagram è
  riuscito compiere un upgrade a Python 3 senza alcun disservizio o
  funzionalità mancante.
  Il lavoro è stato diviso in 3 fasi: i primi 4 mesi sono serviti per la
  sostituzione di pacchetti obsoleti, i seguenti 2 mesi sono stati impiegati
  per lo unit testing che ha permesso loro di non avere nessuna regressione;
  gli ultimi 4 mesi sono stati necessari per il rilascio graduale in produzione.

  Dunque il tempo effettivo per l'upgrade è riducibile a soli 6 mesi di lavoro:
  un risultato eccezionale per l'aggiornamento di una piattaforma enorme e
  complessa come quella di Instagram.

  % TODO python success ILM Industrial Light & Magic

  \subparagraph{5. Futuro di Python} Quando si decide di investire tempo per
  apprendere una nuova tecnologia, si desidera che essa possa essere valida per
  molti anni a seguire. Dopo quanto visto fin'ora, non dovrebbe
  stupire che Python ha attualmente un futuro incoraggiante di fronte a sé, ma
  andiamo ad approfondire.

  La prima statistica che è necessario verificare per la salute attuale del
  progetto è la storia recente della sua attività, ovvero quanto lavoro è stato fatto
  negli ultimi mesi. Per verificarlo possiamo osservare le statistiche del
  repository git di \textbf{CPython}, l' implementazione ufficiale scritta in C
  \footnote{esistono svariate implementazioni di Python in diversi linguaggi di
  programmazione. Una delle più famose è Jython con la quale il codice Python
  può essere eseguito su piattaforma Java}:

  \begin{figure}
    \includegraphics[width=1.0\textwidth]{img/cpython_repo}
    \caption{Statistiche repository CPython}
    \label{python:cpython_repo}
  \end{figure}

  Il grafico, consultabile da chiunque su Github
  \footnote{https://github.com/python/cpython/graphs/contributors}, mostra
  un dato estremamente confortante. Lo sviluppo dal 1992 è cresciuto fino agli
  anni 2000 e da quel momento è rimasto abbastanza stabile. Il numero di
  contributors ammonta attualmente a 299, numero considerevole vista la
  complessità del progetto.

  La seconda proprietà che è utile verificare, riguarda la capacità del
  linguaggio (e del suo ecosistema) di rimanere al passo con i nuovi argomenti
  di tendenza nel settore tecnologico, dimostrando di essere reattivo a
  cambiamenti ed innovazioni dell'industria.
  Osservando gli ultimi movimenti nel campo dell'information
  technology, sono estremamente attuali tematiche come Internet of Things,
  robotica, maker culture ed intelligenze artificiali.
  Python si dimostra nuovamente un linguaggio non solo all'altezza, ma anche
  uno dei più promettenti. Prendiamo come
  esempio quanto fatto dalla \textbf{Raspberry Pi Foundation} la quale, tramite
  il progetto Picademy, incoraggia lo studio delle scienze informatiche nelle scuole
  ed offre corsi per apprendere il linguaggio Python che verrà poi utilizzato
  per interagire con le versatili board programmabili dell'organizzazione:
  il \textit{"Raspberry Pi"}, un mini-PC completo di numerosi input-output che
  possono essere sfruttati tramite librerie Python.
  Queste piccole schede sono state usate con successo sia per scopi didattici,
  sia nel mondo maker, grazie al basso costo accoppiato con le loro capacità.
  Potendo estendere le funzionalità di un Raspberry Pi tramite periferiche USB,
  oppure tramite sensori ed attuatori collegati attraverso le altre interfacce,
  l'unico limite realizzativo è dettato dall'immaginazione (e dalle basse
  prestazioni offerte dal SoC ARM equipaggiato).

  \begin{figure}[H]
    \includegraphics[width=1.0\textwidth]{img/raspberrypi}
    \caption{Un Raspberry Pi 3}
    \label{rpi}
  \end{figure}

  Cercando in rete è possibile trovare una miriade di progetti basati sul
  RaspberryPi: stazioni meteo, sistemi di sorveglianza domestica, router,
  irrigatori automatici e molti altri.
  Python si rivela uno strumetno completo per qualsiasi esigenza e grazie
  alla vasta libreria software di cui dispone, è semplice realizzare qualsiasi
  tipo di applicazione.

  Riguardo le librerie software, non mancano di certo progetti in grado di
  avventurarsi negli ultimi trend dell'informatica: a tal proposito, merita una
  menzione importante \textbf{TensorFlow}, una libreria software creata da
  Google per fare ricerca nel settore machine learning e reti neurali; questo
  software è stato rilasciato con licenza open source nel 2015 offrendo un'API
  per Python.

  \paragraph{}
  Da quanto osservato fin'ora, sopratutto ripensando alla storia di successo
  di Instagram, si può concludere che studiare Python non è assolutamente un
  azzardo, ma risulta una scelta fondamentale per la crescita individuale:
  questo linguaggio ha dimostrato di non abbandonare mai lo sviluppatore,
  fornendogli soluzioni variegate in ogni settore.
