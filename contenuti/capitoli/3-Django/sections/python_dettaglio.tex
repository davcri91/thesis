\section{Python in dettaglio}
  In questa sezione andremo ad esplorare le caratteristiche peculiari di Python,
  nella versione 3.6. Introdurremo concetti che saranno utili per comprendere
  alcune soluzioni che verranno utilizzate nel capitolo finale per mostrare
  come sono state implementate le funzionalità di progetto, sfruttando tutte
  le capacità del linguaggio Python.

  Inoltre introdurremo il concetto di \textit{Pythonic code}, mostrando alcuni
  idiomi unici di Python e, facendo confronti, mostraremo l'eleganza del codice
  Python.


  % ========================================
  % TODO Inserire tutte le funzionalità peculiari di Python che verranno
  % utilizzate in SoftHub. In maniera da introdurre i concetti che verranno
  % utilizzati nel capitolo finale

  % TODO esempio:
  % - Meta class (https://stackoverflow.com/questions/100003/what-is-a-metaclass-in-python)
  % - decorator
  % ========================================


  Python è un linguaggio dalla sintassi molto particolare per chi è abituato ai
  classici studi dell'informatica con C++, Java e similari. Il codice prodotto è
  estremamente compatto e leggibile. Altra caratteristica importante è la sua
  tipizzazione dinamica: Python non richiede di esplicitare il tipo di
  variabile in fase di definizione.
  Molto spesso gli script Python risultano quasi leggibili per come risultino
  simili al linguaggio umano:

\begin{minted}[frame=lines, linenos]{python}
  lista_invitati = ["Jack", "John", "Mave", "Paul"]
  nuovo_arrivato = "Maccio Capatonda"

  if nuovo_arrivato in lista_invitati:
     print("Benvenuto")
  else:
     print("Mi dispiace, non sei stato invitato")
\end{minted}

  Dal precedente esempio si può apprezzare l'estrema eleganza di Python.
  C'è da sottolineare che il codice riportato è completo, non manca di un "main"
  o di alcun import di librerie: l'ambiente Python è dotato del supporto a
  stringhe, liste, funzioni di i/o \footnote{Input/output}.

  Solo per fare un veloce e curioso confronto, il codice analogo in C++ sarebbe il
  seguente:

\begin{minted}[frame=lines, linenos]{c++}
#include <iostream>
#include <list>
#include <string>

int main ()
{
  std::list<std::string> mylist {"Jack", "John", "Mave", "Paul"};
  std::string nuovo_arrivato = "Jack";
  bool invitato_presente_in_lista = false;

  for (std::list<std::string>::iterator it=mylist.begin();
      it!=mylist.end();
      ++it)
    if (nuovo_arrivato == *it){
      invitato_presente_in_lista = true;
      break;
    }

  if (invitato_presente_in_lista)
    std::cout << "Benvenuto";
  else
    std::cout << "Mi dispiace, non sei stato invitato";

  return 0;
}
\end{minted}

  La prime cose che saltano all'occhio sono la differenza notevole in linee di
  codice e la difficile sintassi C++, che risulta difficilmente leggibile anche
  da chi ha studiato il linguaggio. Solo per scrivere e compilare con
  successo questo semplice esempio C++ ho impiegato 25 minuti, a causa di continui
  errori di sintassi, namespacing, parentesi graffe dimenticate e librerie
  mancanti.

  Python ha deciso di puntare tutto sul'estrema semplicità, riducendo lo sforzo
  cognitivo necessario allo sviluppatore per interfacciarsi con un calcolatore.
  Il risultato è un linguaggio più simile a quello che utilizziamo
  quotidianamente piuttosto che ad un linguaggio macchina. Per ottenere questo
  risultato, Guido van Rossum decise saggiamente di eliminare tutte quelle parti
  superflue nella sintassi, cercando di mantenere una coerenza logica nella
  struttura del programma. Tutte le regole di stile necessarie per programmare
  in questo linguaggio sono documentate nel PEP 8 \cite{software:python:pep8},
  dove vengono espresse le regole sintattiche che l'interprete richiederà
  obbligatoriamente, le buone pratiche ed anche le raccomandazioni da seguire
  nei casi limite.

  I punti salienti di questo documento fanno emergere alcune caratteristiche
  della sintassi: Python utilizza obbligatoriamente 4 spazi per l'indentazione
  del codice, rifiutando di eseguire qualsiasi codice che non rispetti questa
  regola. Allo stesso modo si rifiuterà di eseguire file con spazi e tab
  utilizzati contemporaneamente; chi ha collaborato in un team di sviluppo, sa
  esattamente quanto possa essere frustrante condividere codice con chi utilizza
  uno stile piuttosto che l'altro; Python standardizza anche questo aspetto,
  minimizzando i tempi sprecati in diatribe inutili che portano solo a
  procrastinare. Nel PEP 8 vengono anche fornite delle linee guida importanti
  come la lunghezza massima di una riga di codice, la quale non deve superare i
  79 caratteri; nel caso in cui non si rispetti questa regola, il codice sarà
  comunque eseguibile ma non sarà ritenuto leggibile dai dai professionisti che
  lavorano con Python. Nel documento viene spiegato in dettaglio come spezzare
  le linee troppo lunghe nei vari casi. Ricordate quando, qualche pagina
  addietro, menzionavo di quanto sia coeso l'ecosistema Python ? Ora è arrivato
  il momento di rafforzare maggiormente quella tesi mostrando due strumenti
  fondamentali che sono reperibili da PyPI: \mintinline{c}{pycodestyle}
  \footnote{\url{https://pypi.python.org/pypi/pycodestyle}} è il primo di questi
  tool, fornisce un programma in grado di prendere in input codice python per
  analizzarlo ed individuare tutti i punti del programma in cui non si sono
  rispettate le regole della style guide di Python. Il secondo tool è
  \mintinline{c}{autopep8}
  \footnote{\url{https://pypi.python.org/pypi/autopep8}} che, sfruttando le
  funzionalità offerte dal primo, è in grado di formattare automaticamente
  il codice Python in modo da fargli rispettare le regole e le buone pratiche
  di PEP 8. Per uno sviluppatore è estremamente rassicurante avere questi
  software di utilità che automatizzano i compiti tediosi dello sviluppo,
  lasciando la mente libera e riposata per lavorare sulle funzionalità più
  complesse del progetto software che abbiamo in cantiere.

  Rispetto ai classici linguaggi, Python rimuove le parentesi graffe,
  l'utilizzo del "punto e virgola" \footnote{Per la gioia di chi non ha una
  tastiera con pulsante dedicato} ed alcune parole chiave. Ciò che ne è rimasto è
  una sorta di pseudocodice che rappresenta l'essenza del programma, la sua
  logica.

  \subsection{Tipi e strutture di dato}
    Python fornisce nativamente tutti i classici tipi di dato e gli affianca con
    strutture dati complesse, il tutto corredato da preziosi metodi di utilità:
    troviamo interi, float, stringhe, booleani che funzionano tutti come ci si
    aspetterebbe, eccezion fatta per i booleani che hanno una particolarità:

\begin{minted}[frame=lines, linenos]{python}
numero_magico = 42  # intero
pi = 3.14           # float
nome = "Davide"     # stringa unicode (in Python 3)

# booleani
python_is_cool = True
juventus_ha_vinto_il_triplete = False
\end{minted}

    Come osservato nell'esempio, i booleani \mintinline{python}{True} e
    \mintinline{python}{False} devono essere scritti con lettera iniziale
    maiuscola, mentre in altri linguaggi vengono scritti con la minuscola. Per le
    strutture dato più complese ci sono liste, tuple, set e dizionari.
    La documentazione ufficiale è estremamente curata e spiega in maniera
    approfondita come utilizzare le funzionalità del linguaggio, per eventuali
    chiarimenti è consiglabile consultarla al seguente indirizzo: \url{https://docs.python.org/3/library/stdtypes.html}.

    Andiamo ora ad illustrare, tramite esempi, il funzionamento basilare dei
    costrutti più utilizzati in Python.

    \begin{itemize}
      \item \textbf{Liste}: sono elenchi di oggetti ed è buona pratica far sì
      che queste contengano dati omogenei:
        \begin{minted}[frame=lines, linenos]{python}
colori = ["red", "green", "blue"]

colori.append("orange")
colori.append("black")
colori.append("white")

colori.remove("orange")

colori.sort()

for colore in colori:
    print(colore)

# OUTPUT:
# > black blue green red white
        \end{minted}

      Nella prima riga abbiamo creato una lista tramite le parentesi quadre.
      Nelle righe successive notiamo come sia possibile aggiungere elementi in
      modo dinamico tramite il metodo \mintinline{python}{append()} e come sia
      possibile fare l'operazione inversa tramite \mintinline{python}{remove()}.
      Il metodo \mintinline{python}{sort()} permette di ordinare la lista; le
      righe 11,12 mostrano come sia semplice scorrere sulla lista colori,
      prendendo l'i-esimo elemento per poterlo sfruttare all'interno del ciclo
      \mintinline{c}{for}, che in Python si comporta in maniera estremamente
      simile ad un \mintinline{c}{foreach} presente in altri linguaggi di
      programmazione.

      \item \textbf{Tuple}: sono simili alle liste, ma sono immutabili ed è
      buona pratica far sì che queste contengano dati eterogenei, a differenza
      delle liste.

      \begin{minted}[frame=lines, linenos]{python}
rosso = "F00"
blu = "00F"
nero = "000"

origine = (0, 0, nero)
punto_asse_x = (1, 0, blu)
punto_asse_y = (0, 4, rosso)

insieme_punti = (origine, punto_asse_x, punto_asse_y)

for punto in insieme_punti:
    coordinate_xy = punto[0:2]
    colore = punto[2]

    print("(x,y) = {0} | HEX< = {1}".format(coordinate_xy, colore))

# OUTPUT:
# > (x,y) = (0, 0) | HEX = 000
#   (x,y) = (1, 0) | HEX = 00F
#   (x,y) = (0, 4) | HEX = F00
      \end{minted}

    Nell'esempio abbiamo supposto di dove trattare dei punti su un piano a due
    dimensioni, associando ad ogni punto un'informazione sul colore. Si è quindi
    pensato di utilizzare una tupla che contenesse nelle prime due posizioni, le
    coordinate x,y mentre, sulla terza posizione, una stringa esadecimale per la
    codifica del colore. Le linee 5-7 mostrano come sia possibile inizializzare
    una tupla utilizzando le parentesi tonde; sulla linea 9 si crea una tupla
    contenente le 3 tuple definite in precedenza, mostrando come si possano
    semplicemente concatenare queste strutture dati. Le righe 11-14 servono per
    la stampa dei dati, dove sono stati utilizzati un paio di costrutti che
    potrebbero richiedere ulteriore spiegazione: all riga 12, l'istruzione
    \mintinline{python}{punto[0:2]} viene chiamata operazione di
    \textit{slicing} in quanto consente di "tagliare" la tupla negli indici
    specificati come argomento: in questo caso si inizia a tagliare dall'indice
    0 e ci si ferma prima dell'indice 2, ottenendo gli elementi in posizione 0,1
    della tupla che corrispondono esattamente alle coordinate x,y che cercavamo.
    Nella riga 14 si utilizza l'operatore \mintinline{c}{format} \footnote{ Per
    la documentazione estesa, vedere qui
    \url{https://docs.python.org/3.6/library/string.html\#format-examples}} che
    consente di inserire all'interno della stringa da mandare in output, il
    contenuto delle variabili.

    \item \textbf{Set}: sono insiemi di elementi non ordinati e privi di
    duplicati. Vengono utilizzati per rimuovere duplicati dalle sequenze o
    per eseguire operazioni matematiche come intersezioni, unioni e differenze.

\begin{minted}[frame=lines, linenos]{python}
A = {1, 2, 3, 4, 5}
B = {4, 5, 6, 7, 8}

print(A.union(B))
# {1, 2, 3, 4, 5, 6, 7, 8}

print(A.intersection(B))
# {4, 5}

print(A.difference(B))
# {1, 2, 3}

print(A.symmetric_difference(B))
# {1, 2, 3, 6, 7, 8}

# In alternativa ai metodi visti sopra
# si possono utilizzare gli operatori "|", "-", "^"

print(A | B)  # Union
# {1, 2, 3, 4, 5, 6, 7, 8}

print(A & B)  # Intersection
# {4, 5}

print(A - B)  # Difference
# {1, 2, 3}

print(A ^ B)  # Symmetric difference
# {1, 2, 3, 6, 7, 8}
\end{minted}

    \item \textbf{Dizionari}: sono collezioni di elementi, accessibili tramite
    una chiave; è possibile utilizzare qualsiasi tipo di dato sia come chiave
    che come valore associato, per questo i dizionari sono estremamente
    versatili. Per creare un dizionario è sufficiente inserire una lista di
    coppie \mintinline{python}{chiave: valore} separate da una virgola il tutto
    racchiuso da parentesi graffe.

    \begin{minted}[frame=lines, linenos]{python}
esami = {
    'Analisi I': 25,
    'Elettrotecnica': 30,
    'FIsica II': 18
}

somma_voti = 0
for esame, voto in esami.items():
    somma_voti += voto

    if voto is 30:
        print("Bravo hai preso un ottimo voto in {0}".format(esame))

numero_esami = len(esami)
media = somma_voti / numero_esami

print('Media voti = {0}'.format(media))
# Output:
# -> Complimenti hai preso un ottimo voto in Elettrotecnica
#    Media voti = 24.3
     \end{minted}

    Questo codice calcola la media degli esami contenuti all'interno del
    dizionario definito nella prima riga.
    La definizione è abbastanza semplice, la prima particolarità si
    può trovare alla riga 8 dove comincia il ciclo \mintinline{c}{for}: per
    scorrere il dizionario avendo a disposizione sia la chiave che il valore,
    si è utilizzato il metodo \mintinline{c}{items()} che restituisce una
    rappresentazione del dizionario adatta a questo scopo. Senza sfruttare
    questo metodo, avremmo potuto utilizzare il seguente codice:

        \begin{minted}[frame=lines, linenos]{python}
for esame in esami:
    voto = esami[esame]
    somma_voti += voto
        \end{minted}

    le parentesi quadre sono l'operatore con il quale è possibile accedere al
    valore associato alla chiave fornita, ma in seguito vedremo come il metodo
    \mintinline{c}{get()} sia considerato un'alternativa migliore per accedere
    al contenuto di un dizionario.
    \end{itemize}


    \subsection{Pythonic code} Dopo aver introdotto le caratteristiche
    principali del linguaggio, è doveroso introdurre l'importante concetto di
    \textit{Pythonic code}. Questo termine si utilizza per descrivere un codice
    Python che sfrutta tutte le funzionalità e gli idiomi
    \footnote{\url{https://en.wikipedia.org/wiki/Programming_idiom}} peculiari
    del linguaggio. L'obiettivo di questa pratica è produrre un codice che sia
    il più chiaro, conciso e manutenibile possibile. Avviene spesso che sezioni
    di un programma Python siano corrette sia sintatticamente che logicamente,
    ma non risultino Pythonic per colpa di retaggi legati ad altri linguaggi
    di programmazione dove non è possibile, per limiti funzionali,
    realizzare del codice maggiormente leggibile. Per avere delle linee guida
    su come un programma Python debba essere scritto, è possibile fare
    riferimento curioso PEP 20 sullo Zen Python \cite{software:python:pep20}:
    questo documento consiste in una serie di aforismi che esprimono la
    filosofia dietro un buon codice Pythonic. È simpatico ricordare che è
    possibile accedere al contenuto di questo documento tramite un easter
    egg presente in Python: basta importare la libreria \mintinline{c}{this}
    da una shell Python interattiva, come mostrato di seguito:

    \begin{figure}
      \centering
      \includegraphics[width=0.9\textwidth]{img/python/zen}
      \caption{Lo Zen di Python disponibile da shell}
      \label{python:zen}
    \end{figure}


    Andiamo quindi ad esplorare alcuni degli esempi Pythonic di uso più comune;
    così facendo mostreremo come ottimizzare alcuni frammenti di codice Python e
    allo stesso tempo, introdurremo concetti che saranno necessari per
    comprendere alcune soluzioni che verranno mostrate nei seguenti capitoli,
    dove vedremo come sia Django che l'applicazione di tesi realizzata, abbiano
    giovato dall'esistenza di queste buone pratiche.

    \paragraph{Swapping variables} L'esempio più semplice per mostrare in cosa
    consiste la filosofia Pythonic è il classico esempio di swapping del
    contenuto di due variabili. Nella maggioranza dei linguaggi di programmazione,
    è necessario eseguire uno pseudocodice del genere:

    \begin{minted}[frame=lines, linenos]{python}
temp = a
a = b
b = temp
    \end{minted}

    In python è possibile scrivere un codice equivalente in una singola linea
    di codice, assegnando il valore di più variabili con una sola istruzione:

    \begin{minted}[frame=lines, linenos]{python}
a,b = b,a
    \end{minted}


    \paragraph{List comprehension} si tratta di un costrutto sintattico che
    permette di creare una lista, a partire da un'altra lista preesistente;
    questo tipo di paradigma deriva dalla teoria degli insiemi matematici. Per
    ottenere un insieme dei primi 40 numeri pari, abbiamo la seguente
    espressione:

    \[
      {\displaystyle \{n \mid (k\in [0, 40)) [n=2k]\}}
    \]

    in codice Python si traduce in:

    \begin{minted}[frame=lines, linenos]{python}
# Da NON fare
pari = []
for i in range(40):
    pari.append(i*2)

# Pythonic code
pari = [2*k for k in range(40)]
    \end{minted}

    \paragraph{Looping} Per scorrere una sequenza, è possibile utilizzare un
    ciclo while con una variabile che funga da contatore, come si è solito fare
    nei corsi di fondamenti di informatica, oppure utilizzare un approccio più
    pythonic sfruttando il funzionamento del ciclo for:

\begin{minted}[frame=lines, linenos]{python}
cities = ['rome', 'new_york', 'venezia']

# Da NON fare
i = 0
while i < len(cities):
    print(i, cities[i])  # Output > 1 rome 2 new_york 3 venezia
    i += 1

# Pythonic code
for index, city in enumerate(cities):
    print(index, city)  # Output > 1 rome 2 new_york 3 venezia
\end{minted}

    L'output dei due cicli è lo stesso, ma il secondo è più leggibile ed è più
    veloce, permettendo di risparmiare 2 righe di codice.
    La funzione \mintinline{c}{enumerate} prende una sequenza e ritorna un oggetto
    che ad ogni ciclo fornisce l'iterazione attuale ed il valore associato.
    Questo permette di dimezzare le linee di codice necessarie, allocando
    anche una variabile in meno.

    \paragraph{Decorators} sono delle funzioni, chiamate \textit{decoratrici},
    che vengono collegate, tramite una sintassi particolare, ad altre funzioni
    per alterarne dinamicamente il comportamento. Il nome di questo
    costrutto è simile al design pattern definito dalla \textit{Gang of Four} in
    quanto ne condivide alcune caratteristiche, ma non va confuso con
    esso come spiegato nel PEP 318 \cite{software:python:pep308}. Vediamone un
    esempio per capire la sintassi ed il funzionamento.

    Supponiamo di avere un'applicazione web e di dover eliminare l'account di
    un utente, controllando prima se esso sia autenticato.
    Vogliamo anche che la funzione sia generica e riutilizzabile. Vediamo
    come i decorators ci aiutino sia nell'avere un codice riutilizzabile,
    sia nell'eleganza del codice finale:

    \begin{minted}[frame=lines, linenos]{python}
def login_required_decorator(func):
    def wrapper():
        print("Verifico l'autenticazione...")

        # Supponiamo che il framework metta a disposizione un
        # metodo statico con cui accedere alla sessione
        if Framework.user_is_logged_in():
            print("Autenticazione ok!")
            func()
        else:
            print("Permesso negato")

    return wrapper


@login_required_decorator
def rimuoviAccount():
    # codice per rimuovere l'account...
    print("Account rimosso")


rimuoviAccount()

# Output:
# -> Verifico l'autenticazione...
#    Autenticazione ok!
#    Account rimosso
    \end{minted}

    Osserviamo le righe 16-19 dove troviamo una classica funzione Python, con
    una semplice differenza nella riga che precede l'intestazione, dove troviamo
    la sintassi per aggiungere un decorator (o più di uno) ad una funzione. Il
    simbolo '@' serve proprio ad indicare che la stringa seguente sarà il nome
    della funzione decoratrice. Tornando alle prime righe del programma,
    troviamo proprio la definizione del decorator; si tratta di una normale
    funzione Python che deve ritornare un oggetto \textit{callable}, dunque
    invocabile; il parametro \mintinline{c}{func} contiene la funzione che si
    sta decorando (\mintinline{c}{rimuoviAccount} in questo caso).

    Per definire il comportamento del decorator, è sufficiente restituire un
    oggetto callable, che in questo caso è la funzione \mintinline{c}{wrapper}:
    al suo interno troviamo un controllo (fittizio) sull'autenticazione, se il
    check è superato allora viene invocata la funzione, altrimenti non si fa
    nulla e si stampa un messaggio di errore.
    Più avanti vedremo anche come Django fornisca decorators già implementati
    , tra cui troviamo anche \textit{login\_required}, da utilizzare
    semplicemente all'interno dell'applicazione.

    \paragraph{Classi} un'altra particolarità di Python è la sua compattezza
    nella scrittura delle classi. In questo linguaggio non esiste il concetto di
    \textit{public} e \textit{private} né per la visibilità dei metodi, né
    per la visibilità delle variabili.

    Vediamo un breve esempio che mostri i concetti base che verranno sfruttati
    nella maggior parte del codice Django:

    \begin{minted}[frame=lines, linenos]{python}
class Persona():
    def __init__(self, nome):
        self.nome = nome

    def presentati(self):
        print("Salve a tutti, sono {0}".format(self.nome))


class Dipendente(Persona):
    def __init__(self, nome, ditta):
        super().__init__(nome)
        self.ditta = ditta

    def __str__(self):
        return "Dipendente [nome={0}]".format(self.nome)

    def presentati(self):
        super().presentati()
        print("Sono un dipendente {0}".format(self.ditta))


mario = Persona("Mario")
print(mario)  # <__main__.Persona object at 0x7f122fb2ab00>
mario.presentati()  # Salve a tutti, sono Mario

jack = Dipendente("Jack", "Google")
print(jack)  # Dipendente [nome=Jack]
jack.presentati()
# Salve a tutti, sono Jack
# Sono un dipendente Google
    \end{minted}

    Il codice è sufficientemente autoesplicativo per chi ha nozioni di
    programmazione orientata agli oggetti, eccezion fatta per alcune peculiarità:

    \begin{itemize}

      \item I metodi della classe devono avere come primo parametro la parola
      chiave \textbf{self}, che è l'equivalente del puntatore
      \textit{this} in altri linguaggi.

      \item Gli attributi di classe possono essere inizializzati in qualsiasi
      punto, anche all'interno dei metodi di classe come avviene nelle righe
      3 e 12.

      \item I metodi che sono circondati dai caratteri di underscore,
      come \mintinline{c}{__init__}, sono dei metodi speciali
      \footnote{\url{https://docs.python.org/3.6/reference/datamodel.html\#special-method-names}}.
      Facendo l'overriding di questi metodi è possibile plasmare la classe
      in base alle proprie esigenze, ad esempio ridefinendo gli operatori.
      Questo concetto è molto importante quando si utilizza un framework;
      a causa del principio di Inversion of Control, i framework
      utilizzaranno spesso i metodi speciali delle classi che creeremo per
      ottenere informazioni ed eseguire operazioni su di esse.

      \item Per \textit{ereditare} da un'altra classe è sufficiente specificare
      il nome della classe stessa, all'interno delle parentesi tonde
      dell'intestazione, come avviene per la classe Dipendente alla riga 9.

      \item Il metodo \mintinline{c}{__init__} è il costruttore di classe.
      Nella riga 11 si utilizza il metodo \mintinline{python}{super()} per
      accedere alla classe padre ed invocare il costruttore di Persona.

      \item Il metodo \mintinline{c}{__str__} viene invocato quando si
      prova a stampare un'istanza di classe. La sua implementazione di default
      risulta poco informativa (riga 23 per vederne l'output).
      Facendone un'ovverriding (righe 14-15) è possibile fornire un output
      più leggibile (riga27)


    \end{itemize}


    % TODO Meta class ? https://stackoverflow.com/questions/100003/what-is-a-metaclass-in-python
    % https://www.youtube.com/watch?v=VBokjWj_cEA
    % TODO Property getter setter ?
    % TODO Try catch statement ?


    \label{pythonic:keyword_arguments} \paragraph{Keyword arguments} Nelle
    funzioni (e metodi) Python si fa distinzione tra \textit{positional
    arguments} e \textit{keyword arguments}. I primi sono i classici parametri
    che incontriamo anche negli altri linguaggi, mentre i secondi sono quei
    parametri passati ad una funzione tramite l'associazione di un nome
    \footnote{nel mondo dell'informatica sono chiamati anche come \textit{Named
    Parameters}}. Questa funzionalità è, infatti, sfruttata internamente da
    Django per diverse sue funzionalità, tra cui la costruzione e salvataggio
    automatico di oggetti Model sul database. Vediamone un esempio:

    \begin{minted}[frame=lines, linenos]{python}
def foo(pos1, pos2, *args, **kwargs):
    print("Positional arguments = {0}, {1}".
        format(pos1, pos2))
    print("Positional arguments opzionali = {0}".format(args))
    print("Keyword arguments opzionali = {0}".format(kwargs))

foo(1, 2, 'jolly', test=50)

# Output:
# -> Positional arguments = 1, 2
#    Positional arguments opzionali = ('jolly',)
#    Keyword arguments opzionali = {'test': 50}
    \end{minted}

    Il vantaggio principale dei keyword arguments si ha quando si chiamano
    le funzioni: il codice sarà più leggibile e non imporrà agli sviluppatori
    di ricordare l'ordine con cui passare gli argomenti.

        \begin{minted}[frame=lines, linenos]{python}
rinominaFile(
    "IMG_312.png",
    "mio_cane.png",
    True)

rinominaFile(
    fileAttuale="IMG_312.png",
    nuovoNome="mio_cane.png",
    cancellaVecchioFile=True
)
        \end{minted}

Nella prima riga non è chiaro quale sia il ruolo dei singoli parametri. Si può
tentare di interpretarli, ma comunque sarà necessario leggere la documentazione
oppure eseguire la funzione per valutarne il risultato.
Nel secondo caso, è autoesplicativo il ruolo svolto da ogni componente della
chiamata a funzione.

\paragraph{Named Tuples}: supponiamo che all'interno di un ampio progetto
software, ci venga assegnata la responsabilità di realizzare una funzione
Python che esegua una serie di test e ritorni una tupla contenente due
valori: il numero di test eseguiti ed il numero di test falliti; l'unico
vincolo richiesto è l'assenza di print statement all'interno della funzione.
Lavorando in un team, è essenziale scrivere codice che sia il più leggibile
possibile, in questo caso, l'utilizzo delle \mintinline{c}{namedtuple} è di
grande aiuto in quanto permette agli altri sviluppatori di capire l'esito
del test semplicemente dall'output, senza nemmeno dover leggere la
documentazione, o peggio, il codice interno del metodo che realizzeremo.
Vediamo un esempio di implementazione:

\begin{minted}[frame=lines, linenos]{python}
from collections import namedtuple

# definizione di alcuni metodi omessa

def test_mod():
  doTests()
  tests_number = getNumberOfTests()
  failed_tests = getFailedTests()
  return (tests_number, failed_tests)

print(test_mod())
# Output -> (4, 1)

def test_mod_improved():
  doTests()
  tests_number = getNumberOfTests()
  failed_tests = getFailedTests()
  TestResult = namedtuple('TestResult', ['TestsNumber', 'FailedTests'])
  return TestResult(tests_number, failed_tests)

print(test_mod_improved())
# Output -> (TestsNumber=4, FailedTests=1)
\end{minted}

La prima versione, definita sulla linee 5-12, fornisce un output
estremamente scarno, ritornando semplicemente una coppia di valori ambigui.
La seconda versione, definita sulle linee , risulta maggiormente leggibile,
rispettando i requisiti richiesti dal team: qualsiasi operazione si
effettuerà in futuro sulla tupla restituita funzionerà senza problemi in
quanto le \mintinline{c}{namedtuple} sono compatibili con tutte le
operazioni delle tuple.

    % TODO parametri opzionali e con valori di dafult
