\section{Cos'è un framework}

\paragraph{}

  Un framework è un'\emph{architettura software} che supporta gli
  sviluppatori, mettendo a disposizione un ambiente ricco di
  \emph{funzionalità generiche} atte a semplificare il processo di
  realizzazione dell'applicazione, garantendo una struttura solida, collaudata
  e facilmente espandibile.
  Essendo un concetto generico, si possono trovare frameworks software per
  molti campi applicativi: videogame, applicazioni desktop, applicazioni
  per dispositivi mobili, web back-end, web front-end e molti altri.

  Lo SWEBOK \cite[sezione 2-5]{software:SWEBOK} (Software Engineering Body of
  Knowledge) è uno standard internazionale che specifica una guida alle
  conoscenze teoriche generalmente accettate nel campo del Software Engineering;
  al suo interno possiamo trovare un'ottima definizione di framework legata alla
  programmazione ad oggetti:

  \textit{"In object-oriented (OO) programming, a key related notion is that of a
  framework: a partially completed software system that can be extended by
  appropriately instantiating specific extensions (such as plug-ins)."}

  Un framework è dunque un software completato parzialmente che può essere
  esteso creando delle opportune classi che si interfaccino con quelle del
  framework. Le basi del framework sono costituite dunque dal linguaggio in cui è
  scritto e dalle librerie software disponibili, ma non si tratta di una
  semplice raccolta di esse: verranno utilizzate per eseguire compiti di basso
  livello ma lo sviluppatore non le richiamerà direttamente dal proprio codice e
  non sarà a conoscenza di come queste librerie vengano utilizzate \footnote{In
  realtà, se il software è rilasciato con una licenza open source sarà possibile
  leggere il codice sorgente e studiarne il funzionamento}. Per implementare i
  requisiti funzionali di progetto, basterà conoscere l'API (\textit{Application
  Programming Interface})
  % TODO GLOSSARIO: API
  del framework, la quale servirà a nascondere tutta la complessità di
  implementazione e, allo stesso tempo, fornirà classi, metodi e funzioni di
  alto livello per poter controllare ed estendere il comportamento del
  framework stesso.

\section{Sviluppo con e senza framework}
  I pensieri spontanei che possono sorgere nella mente di uno sviluppatore
  alle prime prese con strumenti così elaborati sono:
  \begin{quote}
    "So già implementare queste funzionalità autonomamente senza l'ausilio di
    codice scritto da altri, perché dovrei utilizzare un framework ?"
  \end{quote}
  \begin{quote}
    "Varrà veramente la pena investire tempo nello studio della documentazione
    di un framework ?"
  \end{quote}

  Spesso lo studio di un framework può rivelarsi complicato, per via della
  mole di codice e documentazione da studiare. Potrebbe risultare addirittura
  controproducente nei primi tempi ma, sul lungo andare, si dimostrerà
  una scelta fruttuosa.

  \paragraph{} \textit{Nota: ciò che	verrà scritto di seguito sarà valido in
  generale, ma andremo ad approfondire concetti specifici del mondo di sviluppo
  web back-end, in particolare analizzeremo il framework web \emph{Django}.}

  Supponiamo di dover sviluppare un'applicazione web per un contratto di
  lavoro. Supponiamo inoltre di non avere vincoli nei requisiti tecnici: i
  primi passi che dovremo compiere saranno quelli di individuare i requisiti
  funzionali di sistema e scegliere la tecnologia da utilizzare per
  l'implementazione. Analizziamo due vie percorribili:

  \subsection{Via autonoma}
  Nella prima via, rinunciamo all'utilizzo di un
  framework web e dunque passiamo alla scelta di un linguaggio di
  programmazione di nostra conoscenza e qualche libreria di
  supporto. Da questo punto in poi dovremo costruire blocco su blocco l'intera
  applicazione software, la quale dovrà rispettare parametri di qualità come
  efficienza, scalabilità, affidabilità e robustezza.
  % TODO inserire in appendice o nel glossario questi termini specifici
  % della qualità del software https://it.wikipedia.org/wiki/Qualit%C3%A0_del_software

  Per farlo, ci assumeremo un insieme di responsabilità che possiamo
  classificare come segue:
  \begin{enumerate}
    \item \textbf{Progettazione}: identificare le classi di dominio ed
    identificare qual'è il pattern architetturale più adatto
    \item \textbf{Implementazione}: implementare l'architettura scelta nel
    punto precedente, implementare funzionalità di base come
    autenticazione, routing, ed interfaccia amministratore; progettare uno
    schema per il database ed implementare uno strato software in grado di
    dialogare con il DBMS
    \item \textbf{Documentazione}: creare e mantenere aggiornata la
    documentazione di
    progetto, configurare un ambiente eseguibile con istruzioni e guide su
    come installare dipendenze, configurare il progetto ed eseguire
    l'applicativo. Questo si rende necessario sia quando si lavora da soli, ma
    sopratutto quando si lavora in un team di lavoro, dove bisogna facilitare la
    fase di setup per essere produttivi il prima possibile.
    \item \textbf{Manutenzione}: come responsabili dell'applicazione dovremo
    assicurarne il funzionamento, garantendo un bugfixing costante e
    soprattutto rilasciare velocemente patch di sicurezza
    \item \textbf{Flessibilità}: in caso di cambiamento dei requisiti, dovremo
    essere in grado di estendere o modificare facilmente il comportamento del
    programma
  \end{enumerate}

  Tutti questi punti sono assolutamente necessari per un progetto in salute, ma
  da soli non bastano per soddisfare le richieste del nostro committente:
  assolvendo le nostre responsabilità fondamentali, non abbiamo implementato
  nemmeno uno dei requisiti funzionali per la sua applicazione ! Le prime
  giornate lavorative sono state esclusivamente dedicate alla preparazione della
  struttura del progetto.

  % TODO da riusare ?
  % Inoltre ci sarebbero potute essere richieste particolari, come
  % il supporto ad una futura \emph{internazionalizzazione} dell'applicazione:
  % quanto lavoro extra avremmo dovuto compiere per supportarla ? È interessante
  % sottolineare che non si tratta di un semplice lavoro di traduzione del testo
  % contenuto nelle pagine, in quanto sarà necessario tener conto dei fusi
  % orari, della codifica dei font, della pluralizzazione delle parole, della
  % traduzione delle URL (e dell'aggiornamento di tutti i reference che saranno
  % probabilmente hard-coded), delle unità di misura e delle valute utilizzate.

  Ci si rende facilmente conto che questi passi preliminari possono sopraffare
  anche lo sviluppatore più esperto.
  Analizziamo la seconda via e cerchiamo
  di capire se può rappresentare una scelta migliore.

  \subsection{Via del framework}
  Consci dell'esistenza di strumenti in grado
  di facilitare la vita dello sviluppatore, andiamo ad analizzare l'attuale
  offerta sul mercato. Si è subito colpiti dal numero di frameworks disponibili,
  di cui molti rilasciati sotto una \emph{licenza open
  source} \footnote{Nel capitolo successivo si analizzerà anche perché molti
  progetti moderni si affidano a questo modello di sviluppo}.

  \begin{center}
    \begin{tabular}{c c c c c}
      \textbf{Python} & \textbf{PHP} & \textbf{Javascript (Node)} &  \textbf{Ruby} & \textbf{Rust} \\
      \midrule
      Django & Laravel & express & Ruby on Rails & Iron \\
      Flask & Symfony & Meteor & & \\
      & Cake PHP & & & \\
      \midrule
      \end{tabular}
  \end{center}

  La tabella riporta una piccola selezione dei frameworks attualmente più
  utilizzati nello sviluppo web. Come fonte, si è fatto affidamento a Github,
  piattaforma di riferimento per gli sviluppatori, la quale ospita una lista
  dei frameworks web più utilizzati ed attivi \footnote{
  \url{https://github.com/showcases/web-application-frameworks} }.

  La prima cosa che salta all'occhio è la presenza di più frameworks per ogni
  linguaggio; questo è subito spiegato dal fatto che ogni framework
  tende a concentrarsi su determinate caratteristiche, in base alla propria
  filosofia di sviluppo.

  Prendiamo i due frameworks python elencati per capire
  in cosa differiscono:
  \textit{Django}, come vedremo in dettaglio nei prossimi capitoli, fornisce
  strutture di supporto avanzate come la validazione delle form, astrazione
  database, supporto all'internazionalizzazione e molto altro; di default offre quindi
  tutte le funzionalità per realizzare un software di grosse dimensioni.
  \textit{Flask} invece è un \emph{microframework}, ridotto all'essenziale per fornire
  solo funzionalità di base; mancano infatti lo strato di astrazione
  del database ed il supporto alla validazione delle form, ma queste
  funzionalità possono essere aggiunte facilmente tramite moduli esterni,
  quando necessario. Questo permette di includere nel proprio progetto
  solamente il codice per le funzionalità che verranno effettivamente utilizzate, ma il
  programmatore dovrà occuparsi di installare e coordinare tutti i moduli
  esterni che andrà ad utilizzare.

  \paragraph{}
  \emph{Scegliere un framework è quindi una questione di filosofia progettuale e di
  esigenze di progetto.} In base ad esse, dovremo
  ragionare attentamente su quale framework puntare; questa scelta risulterà
  molto importante, poiché lo strumento su cui baseremo il nostro codice imporrà
  delle convenzioni di utilizzo e di architettura software che potrebbero non
  adattarsi bene al nostro progetto di lavoro.

  È inoltre interessante osservare come ci sia vasta scelta, qualsiasi sia
  il linguaggio di nostra preferenza. PHP, Python e Ruby sono linguaggi maturi
  ormai da anni utilizzati nel settore web server-side e dunque non stupisce
  che essi siano ricchi di software di supporto; d'altra parte colpisce come
  linguaggi moderni ed in fortissima crescita come ad esempio Javascript
  \footnote{Javascript non è un linguaggio giovane in assoluto, ma qui ci
  si riferisce all'utilizzo di questo linguaggio nell'ambito back-end web dove
  viene eseguito all'interno del runtime \emph{Node.js} \url{https://en.wikipedia.org/wiki/Node.js}},
  Rust e Go possano già fare affidamento su diversi frameworks.

  \paragraph{}
  Torniamo al nostro contratto di lavoro: analizziamo i requisiti richiesti
  e scegliamo il framework web Django, così da avere una base
  solida da cui partire per lo sviluppo della nostra applicazione, sapendo che
  dovremo utilizzare molte funzionalità non incluse in altri frameworks: Flask
  richiederebbe un eccessivo lavoro di configurazione e manutenzione, rendendo
  il framework non adatto al nostro lavoro.

  Riprendiamo le considerazioni fatte
  sulle responsabilità ed analizziamole in questo scenario per vedere come
  cambiano:

  \begin{enumerate}
    \item \textbf{Progettazione}: dovremo identificare comunque le classi di
    dominio, ma il pattern architetturale è stato già scelto da altri
    sviluppatori che hanno maturato esperienza nel settore specifico (web development nel nostro esempio)
    ed hanno scelto l'architettura che meglio si adattasse al contesto ed alla
    filosofia del framework
    \item \textbf{Implementazione}: l'architettura di base è già implementata,
    così come le funzionalità di base (autenticazione, routing,...);
    dovremo progettare lo schema per il database, ma lo strato per la
    persistenza è già implementato ed utilizzabile tramite API
    \item \textbf{Documentazione}: non sarà necessario spiegare il workflow,
    né fornire istruzioni per la configurazione ed installazione del software
    perché è già tutto spiegato nella documentazione del framework;
    andrà documentato solamente il core dell'applicazione
    \item \textbf{Manutenzione}: parte della manutenzione, principalmente
    quella relativa la sicurezza e sui bug dell'API, sarà responsabilità
    del framework; ciò permette di concentrarsi sull'evoluzione dell'applicazione,
    piuttosto che investire tempo nella revisione del codice
    \item \textbf{Flessibilità}: molti frameworks offrono più
    funzionalità di quelle effettivamente necessarie, è molto probabile che
    per estendere la nostra applicazione dovremo aggiungere solamente poche
    righe di codice; quando non ci si trova in questo caso, sarà possibile usare
    moduli esterni in grado di aggiungere altre funzionalità
  \end{enumerate}

  Scegliendo di utilizzare un framework ci si rende conto della mole di lavoro
  che possiamo evitarci e, sopratutto, dell'alleggerimento delle
  responsabilità: molte di queste vengono delegate al framework stesso,
  permettendo allo sviluppatore di concentrarsi sulle funzionalità richieste
  dal committente le quali rappresentano il nucleo dell'applicazione.

  \paragraph{}
  A questo punto non dovrebbero esserci più dubbi:
  affidarsi ad uno strumento conosciuto, su cui molti sviluppatori hanno
  riversato la loro esperienza, porta enormi vantaggi che andremo ad
  esplorare in maggior dettaglio nella prossima sezione.
