\section{Vantaggi offerti da un framework web}

  Nella precedente sezione abbiamo visto come i framework ci aiutino ad
  alleviare alcune responsabilità di realizzazione.
  In questa sezione andremo ad approfondire questi temi esplorando, allo stesso
  tempo, quelli che non abbiamo ancora trattato.

  I browser ed i server web sono applicativi che si trovano nel livello più alto
  della pila protocollare TCP/IP \cite{wiki:internet:tcpip};
  essi implementano il protocollo
  HTTP (Hyper Text Transfer Protocol) basato su un'architettura client/server
  dove l'interazione comune prevede una richiesta da un browser ad
  un'applicazione server eseguita su un computer che ospita un sito web.
  Quando richiediamo il contenuto di un sito web, il browser esegue quindi una
  richiesta al server, scambiando un insieme di informazioni standardizzate nel
  protocollo;

  \begin{center}
    \includegraphics[width=0.8\textwidth]{img/http}
  \end{center}

  un'applicazione server dovrà quindi essere in grado di capire le richieste in
  entrata, elaborare dati, ed inviare una risposta HTTP contenente le
  informazioni richieste dal client: spesso si invierà una pagina HTML, in altri
  casi \footnote{ad esempio una REST API, oppure in una risposta ad una
  richiesta AJAX} potrebbero essere dati formattati in JSON (JavaScript Object
  Notation).

  Realizzare un'applicazione web, richiede dunque anche di occuparsi di uno
  strato software che conosca le regole imposte del protocollo HTTP; quando
  si fa ricorso ad un framework web, tutto questo è già incluso e inoltre avremo
  funzionalità importanti che inglobano le best practices necessarie per
  lavorare con un protocollo stateless.

  \subsection{Funzionalità incluse}
    Il vantaggio più facilmente riconoscibile è quello di avere funzionalità
    implementate out-of-the-box; per uno strumento dedicato alla programmazione
    per il web sono comuni le seguenti:

    \begin{itemize}

      \item \textbf{URL routing}: meccanismo che permette di definire un mapping
      tra l'\textit{URL path} richiesto da un client HTTP ed il codice da
      eseguire. Spesso i
      framework forniscono un file che convenzionalmente verrà utilizzato per
      definire tutte le rotte dell'applicazione: qui verranno specificate anche
      altre opzioni quali i \textit{metodi HTTP} supportati, quali e quanti
      parametri l'URL può accettare, quali rotte richiedono l'autenticazione.

      Un esempio con codice PHP ed il framework Laravel, è il seguente:

% \definecolor{grigio}{rgb}{0.85,0.85,0.85}
\begin{minted}[frame=lines, linenos]{php}
<?php
Route::get('home/', function () {
    return 'Welcome to the home page';
});

Route::get('user/profile/', function () {
    return 'User profile page';
});
?>
\end{minted}

      Nell'esempio ci sono 2 rotte, entrambe supportano solo il metodo HTTP GET
      e non accettano parametri.
      Avere tutte le rotte dell'applicazione all'interno di un unico file
      permette di individuarle facilmente, mantenendo un'organizzazione pulita
      del progetto.

      Molto frequentemente viene fornito anche il supporto alle \textbf{named
      routes} che consentono di assegnare un nome per ogni rotta, permettendo
      di accedervi tramite una funzione specifica.
      Ad esempio supponiamo di avere la nostra home page all'indirizzo
      \textit{"miosito.it/home"}, e supponiamo di aver assegnato il nome
      \textit{"home"} a questa rotta. Il codice del file di mapping diventerebbe
      in questo caso:

\begin{minted}[frame=lines, linenos]{php}
<?php
Route::get('home/', function () {
    return "Benvenuto in una semplice pagina home!"
})->name('home');
?>
\end{minted}

    Per accedere alla rotta \textit{"home"} appena definita, utilizzeremo la
    funzione \mintinline{php}{route()} dell'API del framework:

\begin{minted}[frame=lines, linenos]{php}
<?php
print(route('home'))
# stampa sulla console -> 'miosito.it/home'
?>
\end{minted}

      Questo meccanismo è vantaggioso perché evita la cattiva pratica di hard
      coding delle URL all'interno dei file HTML.
      Utilizzando le named route, avremo un'applicazione estremamente flessibile
      ai cambiamenti dei link: si potranno cambiare facilmente gli indirizzi
      delle URL che invocano determinate sezioni di codice (pensate di voler
      cambiare la rotta da \textit{"home/"}
      in \textit{"welcome/"}): non sarà necessario andare a caccia di ogni file
      HTML contenente delle occorrenze del suddetto link alla home page; basterà
      semplicemente cambiare una stringa nel file di mapping.

      \item \textbf{Database abstraction}: altra importantissima funzionalità
      richiesta da praticamente ogni applicazione web moderna, è la persistenza
      dei dati tramite l'interfacciamento con un DBMS (Database Management
      System). % TODO termine per glossario
      Per soddisfare questa esigenza e, allo stesso tempo, alleviare la
      responsabilità di implementazione da parte degli
      sviluppatori, i framework offrono un layer di astrazione della persistenza
      tramite una semplice API che consente di salvare, modificare e recuperare
      oggetti dal database. Ciò consente di scrivere codice estremamente
      leggibile senza doversi preoccupare di dettagli di basso livello.
      Vediamone un esempio in Django:

\begin{minted}[frame=lines, linenos]{python}
from django.db import models
from .LifePoints import LifePoints

class Giocatore(models.Model):
   username = models.CharField(max_length=50)
   email = models.EmailField()
   lifePoints = models.ForeignKey(LifePoints)

class LifePoints(models.Model):
   value = models.IntegerField()

# Creiamo un oggetto LifePoints
lp = LifePoints()
lp.value = 2000
lp.save()

# Creiamo un oggetto Giocatore
g = Giocatore()
g.username = "Davide"
g.lifePoints = lp
g.email = "davide@mail.it"
g.save()

# Per accedere all'API di Django per database, basta accedere
# all'attributo objects di Giocatore
g2 = Giocatore.objects.last()
print(g2.username, g2.lifePoints.value)
# output: Davide 2000

print(g2)
# output: <Giocatore: Giocatore object>
\end{minted}

      Il codice presentato è estremamente leggibile, anche per chi ha poca
      conoscenza delle tecnologie utilizzate. Le prime due righe sono degli
      import per i moduli \mintinline{python}{models} e
      \mintinline{python}{LifePoints}; le classi che incontriamo successivamente
      estendono la classe generica \mintinline{python}{Model} di Django, questo
      permette loro di accedere alla ricca API di Django per interfacciarsi al
      DB
      \footnote{\url{https://docs.djangoproject.com/en/1.11/topics/db/queries/}}.
      Si deve notare come non si è scritta nessuna riga di codice SQL, nemmeno
      per la creazione dello schema del database, il framework se ne farà pieno
      carico; è sufficiente specificare le relazioni tra le classi tramite dei
      campi specifici appartenenti al modulo \mintinline{python}{django.db.models}.

      I vantaggi non finiscono qui: la maggior parte dei frameworks offre
      supporto ad una varietà di tecnologie database come MySQL, SQLite,
      PostgreSQL, Oracle DB e molti altri. Per passare da un motore ad un altro,
      basterà cambiare un parametro di configurazione all'interno di un file.
      Il core dell'applicazione sarà completamente protetto da variazioni sullo
      strato di persistenza, garantendo un'applicazione estremamente flessibile
      e facilmente evolvibile.

      \item \textbf{Template Engine}: strumento che permette una separazione
      degli interessi tra l'interfaccia (il codice HTML che verrà inviato come
      risposta) ed il back-end.
      Quando il server deve generare una risposta HTTP dovrà includere una
      pagina HTML appropriata. Il linguaggio di templating viene utilizzato per
      variare dinamicamente il contenuto di questa pagina, in
      base allo stato dell'applicazione ed in base all'utente che ha effettuato
      la richiesta. Per fare ciò, il programmatore rende disponibile un
      \textbf{contesto}, ovvero un insieme di variabili, accessibile dal codice
      HTML.
      Ad esempio supponiamo di voler mostrare una lista di 10 film usciti di
      recente al cinema. Il codice Python+Django per fare ciò è il seguente:

\begin{minted}[frame=lines, linenos]{python}
from django.http import HttpResponse
from django.template import loader

class Film(models.Model):
   data_uscita = models.DateField()

# ...
# supponiamo di aver salvato sul DB diverse istanze di Film

lista_film = Film.objects.all()
film_recenti = lista_film.order_by('-data_uscita')[:10]

# context è la variabile di contesto che
# verrà passata al metodo render()
context = {
   'film_recenti': film_recenti
}

template = loader.get_template('templates/film_recenti.html')
response_data = template.render(context, request)
return HttpResponse(response_data)
\end{minted}

\begin{minted}[label=templates/film\_recenti.html, linenos, frame=lines, labelposition=topline]{html}
<html>
{% for film in film_recenti %}
  <h1> {{ film.titolo }} </h1>
  <p> {{ film.descrizione }} </p>
{% endfor %}
</html>
\end{minted}

% \begin{myhtml}{templates/film\_recenti.html}
% <html>
% {% for film in film_recenti %}
%   <h1> {{ film.titolo }} </h1>
%   <p> {{ film.descrizione }} </p>
% {% endfor %}
% </html>\end{myhtml}

      Si può notare come dal codice del back-end inizializziamo una variabile di
      contesto (riga 15) per poi passarla al metodo render (riga 20) il quale
      attiverà, dietro le quinte, il motore di rendering che effettuerà
      tutte le sostituzioni ed operazioni necessarie per creare
      dinamicamente la pagina HTML.
      Queste informazioni saranno utilizzabili all'interno del file
      \textit{film\_recenti.html} tramite una sintassi
      specifica del linguaggio di template.
      Le parentesi angolari doppie \textit{\{\{ \}\}} servono per accedere alle
      variabili di contesto; le parentesi angolari adiacenti ad un simbolo di
      percentuale \textit{\{\% \%\}} indicano invece i \textit{tag} del template
      engine; tramite questi, è possibile accedere ad
      istruzioni condizionali, con le quali è
      possibile costruire una logica anche abbastanza complessa sul front-end.

      Quest'ultimo punto risulta importante quando si lavora in team; chi lavora
      sul front-end richiederà esplicitamente la disponibilità di
      informazioni all'interno di specifiche variabili e su queste specifiche
      richieste andrà a realizzare i templates per l'interfaccia dell'
      applicazione; il team che lavora sul back-end non dovrà fare altro che
      raccogliere i dati dalla persistenza, elaborare le informazioni e fornirle
      creando un contesto di template.

      % TODO
      \item \textbf{Migrations}: sono una sorta di versionamento dello schema
      del database.
      Ogni volta che c'è necessità di un cambiamento sul database, viene creata
      una nuova migration che descrive le modifiche rispetto all'ultima
      versione. Solitamente i frameworks creano un file per ogni migration,
      nominandoli con un numero crescente ed un nome significativo per
      l'operazione effettuata. Il contenuto di questi file è scritto con un
      linguaggio indipendente dal database.

      Avere questo tipo di versionamento risulta una sicurezza per lo
      sviluppatore; se introduciamo dei cambiamenti che rompono l'applicazione,
      sarà possibile eseguire un rollback per tornare ad una versione
      funzionante del database. Altra situazione in cui si apprezza l'esistenza
      di un tale sistema è in caso di sostituzione del server database: essendo
      le migrations scritte in un linguaggio indipendente dalla tecnologia
      utilizzata, sarà possibile generare automaticamente un nuovo database,
      perfettamente compatibile con lo strato di dominio realizzato fino a quel
      momento.

      Utilizzare queste migrations significa introdurre ulteriori passaggi nello
      sviluppo, infatti per modificare lo schema del database sarà necessario
      creare le migrations per poi eseguirle tramite un'utility, ma questo
      sforzo aggiuntivo è perfettamente ripagato dai vantaggi che forniscono.

      \item \textbf{Supporto internazionalizzazione e localizzazione}:
      l'obiettivo di questi mezzi (spesso abbreviati con \textit{i18n}) è
      quello di permettere ad una singola applicazione web di offrire il suo
      contenuto in linguaggi e formati adatti per ogni utente. Se si sta
      sviluppando un'applicazione di grosse dimensioni, è probabile che si debba
      tenere conto di fusi orari, valute e linguaggi differenti. Anche in questo
      caso i frameworks forniscono le strutture di supporto necessarie per una
      corretta separazione degli interessi, permettendo al team di traduttori e
      programmatori di lavorare in parallelo, e permettendo di non preoccuparsi
      di problematiche di temporizzazione per via dei diversi timezone.

      A tal proposito, è saggio utilizzare un sistema conscio delle
      problematiche di temporizzazione delle richieste che arrivano da utenti
      dislocati su tutta la terra. La ragione principale
      è dovuta al \textit{Daylight Saving Time} (DST), sistema utilizzato da
      molte nazioni che impone lo spostamento unitario dell'ora in avanti
      durante la primavera, ed indietro durante l'autunno. Se si lavora con
      l'ora locale, c'è il rischio di avere errori quando avviene il cambio
      dell'ora; questo non è un problema se si sta creando un blog, ma potrebbe
      rappresentare un problema estremamente grave se si effettuano calcoli per
      degli addebiti per dei servizi: ogni anno si calcolerà un'ora in più ed
      un'ora in meno.
      La soluzione a questo problema è l'utilizzo di \textbf{UTC}
      (\textit{Coordinate Universal Time}, è uno standard di tempo che non
      utilizza il DST), all' interno del codice, per poi localizzare l'orario
      soltanto nei template mostrati agli utenti; la maggior parte dei framework
      utilizza internamente questo standard (esempio: Django), in alternativa
      si potrà fare uso dell'API per poter ottenere orari UTC.

%       When support for time zones is enabled, Django stores datetime information in
% UTC in the database, uses time-zone-aware datetime objects internally, and
% translates them to the end user’s time zone in templates and forms.
%
% This is handy if your users live in more than one time zone and you want to
% display datetime information according to each user’s wall clock.
%
% (The pytz documentation discusses these issues in greater detail.) This probably
% doesn’t matter for your blog, but it’s a problem if you over-bill or under-bill
% your customers by one hour, twice a year, every year. The solution to this
% problem is to use UTC in the code and use local time only when interacting with
% end users.

      Per ciò che riguarda la localizzazione, Django fornisce il supporto a
      \textbf{gettext} \cite{wiki:gettext}, un consolidato sistema di
      internazionalizzazione e localizzazione . Basterà inserire le stringhe da
      tradurre all'interno della funzione relativa, come di seguito:

\begin{minted}{python}
  gettext("stringa da tradurre")
  # questa funzione, visto il suo numero cospicuo di utilizzi
  # viene spesso abbreviata con il seguente
  # alias di un solo carattere: _()
  _("stringa da tradurre")
  # equivalente alla prima
  # chiamata a funzione
\end{minted}

      Il framework sarà in grado di produrre dei file con estensione
      \textit{.po}, contenenti le stringhe da tradurre. Questi  potranno essere
      passati ad un team di traduttori i quali, tramite semplici programmi
      grafici come
      \textit{poedit} \footnote{\url{https://en.wikipedia.org/wiki/Poedit}}, andranno
      a creare i file di localizzazione per ogni linguaggio.
      Una volta finito questo lavoro, saranno presenti tanti file quanti saranno
      i linguaggi supportati. Per ogni richiesta HTTP il framework si occuperà
      di caricare il file relativo al corretto linguaggio. Per farlo andrà a
      ottenere informazioni dall'header HTTP \textit{Accept-Language}
      \cite{mdn:http:accept_language}.

      \item \textbf{Data validation}: meccanismo che permette di definire dei
      \textit{validatori}, ovvero funzioni che si occupano di validare i dati
      contenuti all'interno degli attributi delle classi. Una volta configurato
      un attributo con un determinato validatore, il framework si occuperà di
      richiamare il relativo codice durante determinati eventi significativi:
      creazione di una form (per inserire i controlli lato client tramite HTML),
      validazione dei dati grezzi ricevuti tramite HTTP (per controllare che i
      dati siano corretti anche lato server, dato che i controlli lato client
      possono essere evitati semplicemente) e salvataggio di oggetti sul
      database.
      Ogni validatore è altamente riutilizzabile in quanto può essere collegato
      su qualsiasi attributo di classe.
      Molti framework forniscono un insieme di validatori più comunemente
      utilizzati; nel caso di Django \cite{django:validators} vengono forniti
      validatori come Email, URL, Immagini, Regular Expression (un particolare
      validatore configurabile), indirizzi IPv4 ed IPv6.
    \end{itemize}

    Quelle esplorate fin'ora sono solo alcune delle funzionalità che i frameworks
    offrono, ma ce ne sono altre che approfondiremo quando parleremo nello
    specifico del framework Django ed altre che non saranno approfondite per
    limiti di spazio. Menzioni importanti sono però le seguenti:

    \begin{itemize}
      \item \textbf{Form validation}: meccanismo che permette di avere
      validazione sia lato server che lato client tramite
      HTML5. Le informazioni ed i validatori da utilizzare potranno essere
      specificati in un unico luogo, evitando duplicazioni di codice. Spesso
      sarà anche possibile costruire delle form di base (ad esempio per la
      creazione di un'istanza di classe Articolo, nell'ambito di un'
      applicazione blog) a partire dalla semplice
      definizione della classe. Il framework si occuperà di analizzare gli
      attributi della classe, costruendo una form con i campi necessari per
      inizializzare l'oggetto con dati validi.

      \item \textbf{Autenticazione}: molto spesso una parte del framework si
      occupa di gestire la registrazione, l'autenticazione e le autorizzazioni
      degli utenti. Viene in genere fornita una classe \mintinline{python}{User}
      già pronta sul quale è possibile invocare metodi come
      \mintinline{python}{is_logged_in()}, \mintinline{python}{is_admin()},
      \mintinline{python}{get_last_login()}, \mintinline{python}{check_password()}.

      \item \textbf{Paginazione}: molte applicazioni hanno spesso necessità di
      presentare lunghe liste di elementi. In questi casi è pratica comune
      ricorrere alla paginazione, ovvero alla suddivisione di elementi su più
      pagine, piuttosto che presentare una lunghissima lista.
      Il framework fornirà integrazione con il template engine permettendo di
      utilizzare la paginazione aggiungendo pochissimo codice.
      Verranno forniti metodi per impostare il numero di oggetti per pagina,
      per ottenere il numero di pagine disponibili e per ottenere i link
      \textit{"Pagina precedente/successiva"} che verranno generati
      automaticamente.
    \end{itemize}


  \subsection{Vantaggi non funzionali: la qualità più importante} Fin'ora
  abbiamo messo in evidenza tutte le qualità funzionali, ma ci sono altri
  importantissimi vantaggi che sono la conseguenza della scelta di un framework.
  Il più importante è sicuramente il livello qualitativo del software che
  andremo a realizzare.
  L'enorme mole di lavoro ed  esperienza che lo strumento ha accumulato durante
  gli anni di sviluppo sarà la base della nostra applicazione; sono infatti decine, a
  volte centinaia, gli sviluppatori che contribuiscono al codice, alla
  manutenzione ed alla documentazione. Non bisogna assolutamente sottovalutare
  questo aspetto, in quanto il codice è sottoposto ad un raffinamento iterativo
  che porta al conservamento delle scelte software vincenti.

  Questa favorevole situazione può essere descritta alla perfezione con una
  citazione di uno dei più importanti fisici di tutti i tempi, Isaac Newton:
  \begin{center}
    \textit{"If I have seen further, it is by standing on the shoulders of giants."}
  \end{center}

  Anche i migliori programmatori avranno bisogno di "appoggiarsi sulle spalle dei
  giganti", allo stesso modo di come Newton ha fatto durante la sua carriera
  nonostante fosse uno dei migliori fisici mai esistiti.

  Andando ad analizzare la struttura e la composizione di un framework, si può
  tranquillamente affermare che le metaforiche "spalle dei giganti" trovano una
  concretizzazione nelle metodologie di sviluppo, le quali hanno portato
  al successo dei progetti software. Queste sono rappresentate principalmente
  dai \textbf{design pattern}, \textbf{GRASP} e dalle \textbf{best practices}.

  I \textit{design pattern} sono letteralmente delle soluzioni efficienti a
  problemi generici ed estremamente ricorrenti della programmazione ad oggetti;
  ogni design pattern ha un nome specifico ed è corredato da documentazione
  estesa che permette di capire il problema e le varie soluzioni applicabili,
  con relative conseguenze. Data la loro diffusione, è estremamente utile per
  i frameworks riutilizzare nomi dei design pattern come nomi per le classi,
  in maniera da permettere una migliore
  leggibilità del codice, per chi è familiare con i pattern di diffusione
  comune.

  I \textit{GRASP}, General Responsibility Assignment Software Patterns, sono un
  insieme di linee guida che permettono di assegnare e distribuire le
  responsabilità software all'interno di un'applicazione object-oriented. Essi
  costituiscono le fondamenta per costruire i design pattern. In generale,
  quando un framework non utilizza design pattern noti, diventa importante
  comprendere come possano esserci numerose differenti soluzioni ad uno stesso
  problema; la conoscenza dei GRASP, unita ad una flessibilità mentale, saranno
  caratteristiche fondamentali per comprendere la collaborazione tra le classi
  di un framework (così come di una qualsiasi architettura software complessa).

  Infine le \textit{best practices} sono tutte quelle pratiche informali comuni
  nello sviluppo software che sono considerate positive per la qualità del
  codice. Risulta importante avere all'interno del framework, una
  collezione di best practices ritenute utili per l'ambito che si sta trattando.

  Tutta l'esperienza che un framework accumula si traduce in scelte accurate
  di queste 3 componenti che abbiamo appena descritto; di conseguenza, la
  qualità del software che costituirà lo scheletro del nostro progetto, sarà
  impareggiabile.
