#include <iostream>
#include <list>
#include <string>

int main ()
{
  std::list<std::string> mylist {"Jack", "John", "Mave", "Paul"};
  std::string nuovo_arrivato = "Jack";
  bool invitato_presente_in_lista = false;

  for (std::list<std::string>::iterator it=mylist.begin();
      it!=mylist.end();
      ++it)
    if (nuovo_arrivato == *it){
      invitato_presente_in_lista = true;
      break;
    }

  if (invitato_presente_in_lista)
    std::cout << "Benvenuto";
  else
    std::cout << "Mi dispiace, non sei stato invitato";

  return 0;
}
