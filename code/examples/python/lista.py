colori = ["red", "green", "blue"]

colori.append("orange")
colori.append("black")
colori.append("white")

colori.remove("orange")

colori.sort()

for colore in colori:
    print(colore)

# OUTPUT:
# > black blue green red white
