A = {1, 2, 3, 4, 5}
B = {4, 5, 6, 7, 8}

print(A.union(B))
# {1, 2, 3, 4, 5, 6, 7, 8}

print(A.intersection(B))
# {4, 5}

print(A.difference(B))
# {1, 2, 3}

print(A.symmetric_difference(B))
# {1, 2, 3, 6, 7, 8}


# In alternativa ai metodi visti sopra
# si possono utilizzare gli operatori "|", "-", "^"

print(A | B)  # Union
# {1, 2, 3, 4, 5, 6, 7, 8}

print(A & B)  # Intersection
# {4, 5}

print(A - B)  # Difference
# {1, 2, 3}

print(A ^ B)  # Symmetric difference
# {1, 2, 3, 6, 7, 8}
