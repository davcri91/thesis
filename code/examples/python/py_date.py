from datetime import date

python_release = date(1991, 2, 20)  # 2 February 1991
today = date.today()  # 23 June 2017
delta = today - python_release

print(delta.days)         # 9620 days
print(delta.days / 365)   # 26.36 years

pypi_packets = 110671
print(pypi_packets/delta.days)  # ~ 11
