esami = {
    'Analisi I': 25,
    'Elettrotecnica': 30,
    'FIsica II': 18
}

somma_voti = 0
for esame, voto in esami.items():
    somma_voti += voto

    if voto is 30:
        print("Bravo hai preso un ottimo voto in {0}".format(esame))

numero_esami = len(esami)
media = somma_voti / numero_esami

print('Media voti = {0}'.format(media))
# OUTPUT:
# > Complimenti hai preso un ottimo voto in Elettrotecnica
#   Media voti = 24.3
