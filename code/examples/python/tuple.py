rosso = "F00"
blu = "00F"
nero = "000"

origine = (0, 0, nero)
punto_asse_x = (1, 0, blu)
punto_asse_y = (0, 4, rosso)

insieme_punti = (origine, punto_asse_x, punto_asse_y)

for punto in insieme_punti:
    coordinate_xy = punto[0:2]
    colore = punto[2]

    print("(x,y) = {0} | HEX< = {1}".format(coordinate_xy, colore))

# OUTPUT:
# > (x,y) = (0, 0) | HEX = 000
#   (x,y) = (1, 0) | HEX = 00F
#   (x,y) = (0, 4) | HEX = F00
