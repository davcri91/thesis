def login_required_decorator(func):
    def wrapper():
        print("Verifico l'autenticazione...")

        # Supponiamo che il framework metta a disposizione un
        # metodo statico con cui accedere alla sessione
        if Framework.user_is_logged_in():
            print("Autenticazione ok!")
            func()
        else:
            print("Permesso negato")

    return wrapper


@login_required_decorator
def rimuoviAccount():
    # codice per rimuovere l'account...
    print("Account rimosso")


rimuoviAccount()

# Output:
# -> Verifico l'autenticazione...
#    Autenticazione ok!
#    Account rimosso
