class Persona():
    def __init__(self, nome):
        self.nome = nome

    def presentati(self):
        print("Salve a tutti, sono {0}".format(self.nome))


class Dipendente(Persona):
    def __init__(self, nome, ditta):
        super().__init__(nome)
        self.ditta = ditta

    def __str__(self):
        return "Dipendente [nome={0}]".format(self.nome)

    def presentati(self):
        super().presentati()
        print("Sono un dipendente {0}".format(self.ditta))


mario = Persona("Mario")
print(mario)  # <__main__.Persona object at 0x7f122fb2ab00>
mario.presentati()  # Salve a tutti, sono Mario

jack = Dipendente("Jack", "Google")
print(jack)  # Dipendente [nome=Jack]
jack.presentati()
# Salve a tutti, sono Jack
# Sono un dipendente Google
