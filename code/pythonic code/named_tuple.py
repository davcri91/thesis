from collections import namedtuple

def doTests(): pass

def getFailedTests():
    return 1

def getNumberOfTests():
    return 4

def test_mod():
    doTests()
    tests_number = getNumberOfTests()
    failed_tests = getFailedTests()
    tests_number = getNumberOfTests()
    failed_tests = getFailedTests()

    return (tests_number, failed_tests)

print(test_mod())
# Output -> (4, 0)


def test_mod_improved():
    doTests()
    tests_number = getNumberOfTests()
    failed_tests = getFailedTests()

    TestResult = namedtuple('TestResult', ['TestsNumber', 'FailedTests'])

    return TestResult(tests_number, failed_tests)

print(test_mod_improved())
# Output -> (TestsNumber=4, FailedTests=1)
