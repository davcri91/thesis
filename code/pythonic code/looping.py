cities = ['rome', 'new_york', 'venezia']

# Da NON fare
i = 0
while i < len(cities):
    print(i, cities[i])  # Output > 1 rome 2 new_york 3 venezia
    i += 1

# The GOOD way
for index, city in enumerate(cities):
    print(index, city)  # Output > 1 rome 2 new_york 3 venezia
