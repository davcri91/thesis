def foo(pos1, pos2, *args, **kwargs):
    print("Positional arguments = {0}, {1}".
        format(pos1, pos2))
    print("Positional arguments opzionali = {0}".format(args))
    print("Keyword arguments opzionali = {0}".format(kwargs))

foo(1, 2, 'jolly', test=50)

# Output
# > Positional arguments = 1, 2
# Positional arguments opzionali = ('jolly',)
# Keyword arguments opzionali = {'test': 50}
