# Python TIPS for better code

# https://youtu.be/VBokjWj_cEA


###############################
# Enumerate
##############################
# BAD way
cities = ['rome', 'new york', 'venezia']
i = 0
for c in cities:
    print(i, c)
    i += 1

# The GOOD way
for i, city in enumerate(cities):
    print(i, city)

###############################
# Zip function
##############################
x_list = [1,2,3]
y_list = [4,5,6]

# BAD way
for i in range(len(x_list)):
    x = x_list[i]
    y = y_list[i]
    print(x, y)

# GOOD way
for x,y in zip(x_list,y_list):
    print(x, y)



###############################
# get in dictionaries
##############################
# BAD way
ages = {
        'Mario' : 10,
        'Giovanni' : 20
        }

if 'Mario' in ages:
    age = ages['Mario']
else:
    age = 'Null'

# GOOD way
age = ages.get('Mario', 'None')
