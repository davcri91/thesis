## Problema
For the last 15 or 20, years Linux users have been able to find, install and
update software in their computers using tools and online repositories provided
by their distributions. Debian repositories, for example, contain over 43,000
software packages

1. This model of software distribution has many benefits, but also some
limitations. With eight leading distributions, the amount of work a software
vendor to package their software targeting them it’s very considerable.
Ed infatti in pochi creano pacchetti eseguibili specificatamenteper le
distribuzioni Linux. Ciò che avviene nella maggior parte dei casi è che per
ogni software c'è un _mantainer_ che si occupa di compilare ed eventualmente
patchare il software per una specifica distribuzione (specifico OS).

2. Flatpak is a cool new way of creating and distributing images of Linux
applications that run on a wide range of different distributions and run there
in a secure way

3. As of July 2016, the Flatpak framework is in version 0.6.7 and some Linux
apps provide initial support for running under it, including some GNOME and
KDE apps, LibreOffice and Glom.

Currently, the preferred way to distribute Flatpak applications is to export
them to OSTree repositories that can be hosted in standard HTTP servers. They
can also be distributed as single-file bundles.

---

What is missing is a "Linux App Store" (the name is just an example), a central
place for developers to publish their software and for users to easily find it
(directly or in collaboration with desktop applications like Gnome Software).

This document is a design & implementation proposal for a Linux App Store based
on Flatpak bundles.

WHY A LINUX APP STORE?

Below is a list of benefits that a flatpak Linux App Store could provide:

For users:

• Discover and install Linux apps as easily as in other platforms (Android, ...)

• Join a community of users (reviews, questions & answers, ...)

• Securely and easily help the creators of the software they like

• Install new versions of applications in LTS distros

• Change computer and easily install all "my apps" using only his store credentials

For developers:

• Make it easy to distribute apps that run on a wide range of different distros
Non c'è bisogno di creare più pacchetti, di perdere tempo in compilazioni e
testing su varie distro.
Inoltre non bisogna scrivere tante guide, tanti quanti sono i sistemi operativi
supportati.

• "Get me on Linux App Store" badge

• Make easy for users to fund the project (Patreon, Paypal donations, ...)

• Sell the apps or in-app purchases, like in other popular platforms such as Google Play

or Apple App Store

• Obtain direct feedback from users (reviews, comments, ...)

• Make it easy to discover flatpak runtimes/sdks made by others

• Make it easy to distribute flatpak runtimes/sdk in a standard way and know which apps

are using them

Other benefits:

• The centralized store could analyze the flatpak bundles before publishing them in

order to ensure a minimum quality (check namespace, deprecated runtimes, bundled

libs with known security bugs, ...)

• It should be easy to prevent/warn users of running an application with know security

bugs or backdoors, ...
