# Tesi
Documento di progettazione di tesi.  

__Relatore__: Serafino Cicerone  
__Autore__: Davide Cristini  
__Versione documento__ : v5  
__Titolo__: _Django Web Framework: studio e creazione di un’applicazione per la community
open source_

## Compilazione documento Latex
Alcuni pacchetti latex sono particolari e richiedono ulteriori configurazioni
e dipendenze per funzionare:
- **Minted**: (Leggi la [documentazione di minted per l'installazione](https://github.com/gpoore/minted/blob/master/source/minted.pdf)) fornisce highlighting tramite interfacciamento con libreria
*pygmentize* che deve essere installata tramite il comando:  

``` bash
pip install pygments # a quanto pare questo non è sufficiente

pacman -S pygmentize # per Arch Linux
```

Su Atom imposta una **custom toolchain** e aggiungi il parametro `-shell-escape` in
modo da avere la seguente linea:
``` bash
-synctex=1 -interaction=nonstopmode -file-line-error -shell-escape
```
inoltre imposta il **bibTeX compiler** con `biber`


## Note
- L'ombreggiatura delle imamgini è ottenuta tramite GIMP, usando
`Filtri->Luci e Ombra->Proietta Ombra` impostando:
spostamento (2,2), raggio 30, opacità 90
